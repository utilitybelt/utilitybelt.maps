﻿using GrapeCity.Documents.Svg;
using System.Numerics;
using UtilityBelt.Maps.Server.Models;
using UtilityBelt.Navigation.Geometry.Maps;

namespace UtilityBelt.Maps.Server.Services {
    public class DungeonMapGenerator {
        private readonly DungeonService _dungeonService;
        private DungeonMap map;

        public uint Id { get; }

        public DungeonMapGenerator(uint id, DungeonService dungeonService) {
            Id = id;
            _dungeonService = dungeonService;

            map = new DungeonMap() {
                Id = Id
            };
        }

        public DungeonMap? GetDungeonMap() {
            var mapData = new MapLandblockGeometry(Id & 0xFFFF0000, Id & 0x0000FFFF);

            if (!mapData.LandblockGeometry.HasDungeons())
                return null;

            foreach (var chunk in mapData.DungeonChunks) {
                if (chunk.Cells.Count == 0)
                    continue;

                var mapChunk = GetMapChunk(chunk);

                if (mapChunk != null) {
                    map.Chunks.Add(mapChunk);
                }
            }

            return map;
        }

        private DungeonMapChunk? GetMapChunk(DungeonChunkGeometry chunk) {
            var cellIds = chunk.Cells.Select(c => c.CellGeometry.EnvCell.Id).ToList();
            cellIds.Sort();

            var mapChunk = new DungeonMapChunk();

            foreach (var cell in chunk.Cells) {
                var mapCell = MakeMapCell(cell);

                if (mapCell != null) {
                    mapChunk.Cells.Add(cell.CellGeometry.CellId, mapCell);
                }
            }

            if (mapChunk.Cells.Count == 0)
                return null;

            return mapChunk;
        }

        private DungeonMapCell? MakeMapCell(MapCellGeometry cell) {
            var mapCell = new DungeonMapCell(cell.CellGeometry.CellId);

            mapCell.CellStruct = cell.CellGeometry.EnvCell.CellStructure;
            mapCell.EnvironmentId = cell.CellGeometry.Environment.Id;
            mapCell.Origin = cell.CellGeometry.EnvCell.Position.Origin;
            mapCell.Orientation = cell.CellGeometry.EnvCell.Position.Orientation;

            EnsureMapEnvironment(cell);

            return mapCell;
        }

        private void EnsureMapEnvironment(MapCellGeometry cell) {
            if (cell.CellGeometry.EnvCell.EnvironmentId == 0)
                return;

            if (!map.Environments.TryGetValue(cell.CellGeometry.EnvCell.EnvironmentId, out var mapEnvironment)) {
                mapEnvironment = MakeMapEnvironment(cell);
                if (mapEnvironment != null) {
                    map.Environments.Add(cell.CellGeometry.EnvCell.EnvironmentId, mapEnvironment);
                }
            }

            if (mapEnvironment != null && !mapEnvironment.Structures.ContainsKey(cell.CellGeometry.EnvCell.CellStructure)) {
                var envStructure = MakeMapEnvironmentStructure(cell);
                if (envStructure != null) {
                    mapEnvironment.Structures.Add(cell.CellGeometry.EnvCell.CellStructure, envStructure);
                }
            }
        }

        private DungeonMapEnvironment? MakeMapEnvironment(MapCellGeometry cell) {
            return new DungeonMapEnvironment();
        }

        private DungeonMapEnvironmentStructure? MakeMapEnvironmentStructure(MapCellGeometry cell) {
            var structure = new DungeonMapEnvironmentStructure();

            if (!cell.CellGeometry.Environment.Cells.TryGetValue(cell.CellGeometry.EnvCell.CellStructure, out var cellStruct)) {
                return null;
            }

            var polys = cellStruct.PhysicsPolygons;
            var vertices = cellStruct.VertexArray.Vertices;
            float walkableThr = (float)Math.Cos(50 / 180.0f * Math.PI);

            var wallLines = new List<List<Vector3>>();
            List<List<Vector3>> tris = new List<List<Vector3>>();
            foreach (var p in polys.Values) {
                var polyVerts = p.VertexIds.Select(v => vertices[(ushort)v].Origin).ToList();

                bool addedFloor = false;
                for (int i = 2; i < polyVerts.Count; i++) {
                    var a = polyVerts[0];
                    var b = polyVerts[i - 1];
                    var c = polyVerts[i];
                    var norm = CalculateTriSurfaceNormal(a, b, c);

                    if (norm.Z < walkableThr)
                        continue;

                    tris.Add(new List<Vector3>() { a, b, c }.ToList());
                    addedFloor = true;
                }

                if (!addedFloor) {
                    var verts = new List<Vector3>();
                    for (int i = 0; i < p.VertexIds.Count; i++) {
                        var i1 = p.VertexIds[i];
                        var i2 = p.VertexIds[i == 0 ? p.VertexIds.Count - 1 : i - 1];

                        var v1 = vertices[(ushort)i1].Origin;
                        var v2 = vertices[(ushort)i2].Origin;

                        var d = 0.01f;

                        foreach (var plane in cell.CellGeometry.WalkablePlanes) {
                            if (DistanceToPlane(v1, plane) < d && DistanceToPlane(v2, plane) < d && !(verts.Contains(v1) && verts.Contains(v2))) {
                                verts.Add(v1);
                                verts.Add(v2);
                                break;
                            }
                        }
                        if (verts.Count > 0)
                            break;
                    }
                    wallLines.Add(verts);
                }
            }

            var isWeirdCeiling = cell.CellGeometry.EnvCell.EnvironmentId == 0x0D0000CA || cell.CellGeometry.EnvCell.EnvironmentId == 0x0D00016D;

            if (tris.Count > 0 && !isWeirdCeiling) {
                var subject = new Clipper2Lib.PathsD(tris.Select(t => {
                    return new Clipper2Lib.PathD(t.Select(v => new Clipper2Lib.PointD(v.X, v.Y)));
                }).ToList());
                var paths = Clipper2Lib.Clipper.Union(subject, new Clipper2Lib.PathsD(), Clipper2Lib.FillRule.Positive, 2);

                foreach (var path in paths) {
                    var p = path.SelectMany(v => new List<float>() { (float)v.x, (float)v.y }).ToList();
                    p.Add(p.ElementAt(0));
                    p.Add(p.ElementAt(1));
                    structure.FloorPolys.Add(p);
                }
            }

            if (wallLines.Count > 0 && !isWeirdCeiling) {
                foreach (var path in wallLines) {
                    structure.WallLines.Add(path.SelectMany(v => new List<float>() { v.X, v.Y }).ToList());
                }
            }

            return structure;
        }
        public static Vector3 CalculateTriSurfaceNormal(Vector3 a, Vector3 b, Vector3 c) {
            return Vector3.Normalize(Vector3.Cross(b - a, c - a));
        }

        Vector3 ClosestPointOnPlane(Vector3 point, Plane plane) {
            var pointToPlaneDistance = Vector3.Dot(plane.Normal, point) + plane.D;
            return point - (plane.Normal * pointToPlaneDistance);
        }

        float DistanceToPlane(Vector3 point, Plane plane) {
            var closest = ClosestPointOnPlane(point, plane);

            return Math.Abs(Vector3.Distance(point, closest));
        }
    }
}
