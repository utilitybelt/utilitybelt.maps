﻿using ACE.Database.Models.World;
using ACE.DatLoader;
using ACE.DatLoader.Entity;
using ACE.DatLoader.FileTypes;
using ACE.Entity.Enum;
using ACE.Entity.Enum.Properties;
using ACE.Server.WorldObjects;
using GrapeCity.Documents.Svg;
using log4net.Core;
using Messerli.GradientColorPicker;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Options;
using System.Collections.Immutable;
using System.Diagnostics.Metrics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Dynamic;
using System.IO;
using System.Numerics;
using System.Reflection.Metadata;
using System.Text;
using System.Text.Json;
using System.Web;
using UtilityBelt.Maps.Server.Models;
using UtilityBelt.Navigation.Geometry;
using UtilityBelt.Navigation.Geometry.Maps;
using static System.Runtime.CompilerServices.RuntimeHelpers;

namespace UtilityBelt.Maps.Server.Services {
    public class SVGBuilder {
        private readonly MapLandblockGeometry map;
        private readonly GcSvgDocument svg;
        private uint _nextId = 1;
        private SvgGroupElement _patternGroup;
        private int FloorChunkCount = 0;
        private Dictionary<uint, SvgElement> _staticObjectCache = new Dictionary<uint, SvgElement>();
        private List<LandblockInstance> _serverInstances;
        private Dictionary<uint, SvgElement> _textureCache = new Dictionary<uint, SvgElement>();

        private Dictionary<uint, List<uint>> _doorToLeverLookup = new Dictionary<uint, List<uint>>();
        private Dictionary<uint, List<uint>> _leverToDoorLookup = new Dictionary<uint, List<uint>>();
        private SvgDefsElement defs = new SvgDefsElement();

        public SVGBuilder(MapLandblockGeometry map) {
            this.map = map;
            this.svg = new GcSvgDocument();
            var style = new SvgStyleElement();

            Load();

            var g = new SvgGroupElement() {
                Class = "root"
            };

            var minX = float.MaxValue;
            var maxX = float.MinValue;
            var minY = float.MaxValue;
            var maxY = float.MinValue;

            foreach (var chunk in map.DungeonChunks) {
                var chunkGroup = BuildChunk(chunk);

                foreach (var cell in chunk.Cells) {
                    if ((cell.Position.X - 5f) < minX) minX = (cell.Position.X - 5f);
                    if ((cell.Position.Y - 5f) < minY) minY = (cell.Position.Y - 5f);
                    if ((cell.Position.X + 5f) > maxX) maxX = (cell.Position.X + 5f);
                    if ((cell.Position.Y + 5f) > maxY) maxY = (cell.Position.Y + 5f);
                }

                if (chunkGroup != null)
                    g.Children.Add(chunkGroup);
            }

            svg.RootSvg.ShapeRendering = SvgShapeRendering.CrispEdges;
            svg.RootSvg.ViewBox = new SvgViewBox(minX - 2f, -maxY - 2f, maxX - minX + 4f, maxY - minY + 4f);

            style.Children.Add(new SvgContentElement(System.Xml.XmlNodeType.Text) {
                Content = BuildStyles()
            });
            MakePatternGroup();
            defs.Children.Add(_patternGroup);
            for (var i = 0; i < FloorChunkCount; i++) {
                defs.Children.Add(MakeGridPattern(i.ToString()));
            }
            foreach (var kv in _staticObjectCache) {
                defs.Children.Add(kv.Value);
            }

            svg.RootSvg.Children.Add(style);
            svg.RootSvg.Children.Add(defs);
            svg.RootSvg.Children.Add(g);

            var texturesGroups = new SvgGroupElement() {
                ID = "textures"
            };
            texturesGroups.Children.AddRange(_textureCache.Values);
            if (texturesGroups.Children.Count > 0)
                defs.Children.Add(texturesGroups);
        }

        private void Load() {
            _serverInstances = EmuData.GetServerObjects(map.LandblockId);

            foreach (var serverInstance in _serverInstances) {
                var weenie = EmuData.GetWeenieByClassId(serverInstance.WeenieClassId);
                if (weenie == null)
                    continue;

                if ((WeenieType)weenie.Type == WeenieType.Door) {
                    var levelLinks = serverInstance.LandblockInstanceLink.Where(l => l.ParentGuid == serverInstance.Guid);
                    foreach (var link in levelLinks) {
                        var targetLbi = _serverInstances.FirstOrDefault(si => si.Guid == link.ChildGuid);
                        if (targetLbi == null)
                            continue;

                        var targetWeenie = EmuData.GetWeenieByClassId(targetLbi.WeenieClassId);
                        if (targetWeenie == null)
                            continue;

                        if ((WeenieType)targetWeenie.Type != WeenieType.Switch)
                            continue;

                        if (!_doorToLeverLookup.TryGetValue(serverInstance.Guid, out var levers)) {
                            levers = new List<uint>();
                            _doorToLeverLookup.Add(serverInstance.Guid, levers);
                        }
                        if (!_leverToDoorLookup.TryGetValue(link.ChildGuid, out var doors)) {
                            doors = new List<uint>();
                            _leverToDoorLookup.Add(link.ChildGuid, doors);
                        }

                        _doorToLeverLookup[serverInstance.Guid].Add(link.ChildGuid);
                        _leverToDoorLookup[link.ChildGuid].Add(serverInstance.Guid);
                    }
                }
            }
        }

        private SvgGroupElement? BuildChunk(DungeonChunkGeometry chunk) {
            var cells = chunk.Cells.ToList();
            cells.Sort((a, b) => {
                return a.Position.Z.CompareTo(b.Position.Z);
            });

            var g2 = new SvgGroupElement() {
                Class = "chunk",
                ID = $"chunk-0x{cells.First().CellGeometry.CellId:X8}"
            };

            var grid = MapGrid.ByFloors(chunk.Cells);

            var lastZ = "";

            SvgGroupElement levelGroup = null;

            foreach (var chunkGrid in grid.Chunks) {
                var g = new SvgGroupElement() {
                    Class = "floor-chunk",
                    CustomAttributes = new List<SvgCustomAttribute>() {
                        new SvgCustomAttribute("data-chunk-id", chunkGrid.Id.ToString()),
                        new SvgCustomAttribute("data-z", chunkGrid.FloorZ.ToString()),
                        new SvgCustomAttribute("data-cells", string.Join(",", chunkGrid.Cells.Select(c => c.CellGeometry.CellId & 0x0000FFFF)))
                    }
                };

                var floorGroupPath = RenderChunkGridFloors(chunkGrid);
                var wallGroupPath = RenderChunkGridWalls(chunkGrid);
                var staticsGroupPath = RenderStaticObjects(chunkGrid);
                var instancesGroupPath = RenderServerInstances(chunkGrid);

                if (floorGroupPath != null) {
                    g.Children.Add(floorGroupPath);
                }

                if (floorGroupPath != null && wallGroupPath != null && floorGroupPath.Class != "catwalk") {
                    g.Children.Add(wallGroupPath);
                }

                if (staticsGroupPath != null) {
                    g.Children.Add(staticsGroupPath);
                }

                if (instancesGroupPath != null) {
                    g.Children.Add(instancesGroupPath);
                }

                if (g.HasChildren) {
                    if (chunkGrid.FloorZ.ToString() != lastZ) {
                        if (levelGroup != null && levelGroup.Parent == null)
                            g2.Children.Add(levelGroup);
                        levelGroup = new SvgGroupElement() {
                            Class = "floor-level",
                            CustomAttributes = new List<SvgCustomAttribute>() {
                                new SvgCustomAttribute("data-level-z", chunkGrid.FloorZ.ToString())
                            }
                        };
                        lastZ = chunkGrid.FloorZ.ToString();
                        FloorChunkCount++;
                    }
                    g.CustomAttributes.Add(new SvgCustomAttribute("data-z-rel", (FloorChunkCount - 1).ToString()));
                    levelGroup.Children.Add(g);
                }

                try {
                    if (levelGroup != null && levelGroup.Parent == null)
                        g2.Children.Add(levelGroup);
                }
                catch { }
            }

            foreach (var x in g2.Children) {
                foreach (var c in x.Children) {
                    var id = c.CustomAttributes.FirstOrDefault(a => a.AttributeName == "data-chunk-id")?.Value;
                    var cChunk = grid.Chunks.FirstOrDefault(ch => ch.Id.ToString() == id);
                    if (cChunk != null) {
                        var connections = cChunk.Connections.Where(cc => {
                            return g2.Children.SelectMany(gg => gg.Children).Any(g => cc.OtherChunkId.ToString() == g.CustomAttributes.FirstOrDefault(a => a.AttributeName == "data-chunk-id")?.Value);
                        }).ToList();

                        if (connections.Count > 0) {
                            c.CustomAttributes.Add(new SvgCustomAttribute("data-connections", HttpUtility.HtmlAttributeEncode(JsonSerializer.Serialize(connections))));
                        }
                    }
                }
            }

            return g2.HasChildren ? g2 : null;
        }

        private SvgElement? RenderServerInstances(MapChunkGrid chunkGrid) {
            var group = new SvgGroupElement() {
                Class = "instances"
            };

            foreach (var cell in chunkGrid.Cells) {
                BuildServerObjs(group, cell);
            }

            return group.HasChildren ? group : null;
        }

        private SvgElement MakeRectangle() {
            var rectPath = new SvgPathBuilder();
            rectPath.AddMoveTo(false, -5, -5);
            rectPath.AddLineTo(false, -5, 5);
            rectPath.AddLineTo(false, 5, 5);
            rectPath.AddLineTo(false, 5, -5);
            rectPath.AddLineTo(false, -5, -5);

            return new SvgPathElement() {
                PathData = rectPath.ToPathData()
            };
        }

        private SvgReference MakeCellClipPath(SvgGroupElement serverObjsGroup, MapCellGeometry cell) {
            var pos = new Vector2(cell.Position.X, cell.Position.Y);
            var rot = (float)(QuaternionToHeading(cell.CellGeometry.EnvCell.Position.Orientation));
            var world = Matrix4x4.CreateFromQuaternion(cell.CellGeometry.EnvCell.Position.Orientation);

            var x = cell.CellGeometry.WalkablePolys.Select(fp => {
                return new Clipper2Lib.PathD(fp.Select(v => {
                    var t = Vector3.Transform(v, world);
                    return new Clipper2Lib.PointD(t.X, t.Y);
                }).ToList());
            }).ToList();

            var subject = new Clipper2Lib.PathsD(x);
            var paths = Clipper2Lib.Clipper.Union(subject, new Clipper2Lib.PathsD(), Clipper2Lib.FillRule.Positive, 2);

            var pb = new SvgPathBuilder();
            foreach (var path in paths) {
                pb.AddMoveTo(false, (float)path[0].x, (float)-path[0].y);
                for (var i = 1; i < path.Count; i++) {
                    pb.AddLineTo(false, (float)path[i].x, (float)-path[i].y);
                }
                pb.AddLineTo(false, (float)path[0].x, (float)-path[0].y);
            }

            var clipPath = new SvgClipPathElement() {
                ID = $"clip-0x{cell.CellGeometry.CellId:X8}"
            };

            clipPath.Children.Add(new SvgPathElement() {
                PathData = pb.ToPathData()
            });

            defs.Children.Add(clipPath);

            return new SvgReference(clipPath.ID);
        }

        private void BuildServerObjs(SvgGroupElement serverObjsGroup, MapCellGeometry cell) {
            if (!(System.Environment.OSVersion.Platform is PlatformID.Win32NT or PlatformID.Win32Windows))
                AppContext.SetSwitch("System.Drawing.EnableUnixSupport", true);

            var instances = _serverInstances.Where(si => si.ObjCellId == cell.CellGeometry.CellId);

            foreach (var serverInstance in instances) {
                var weenie = EmuData.GetWeenieByClassId(serverInstance.WeenieClassId);

                if (weenie == null)
                    continue;

                var pos = new Vector2(serverInstance.OriginX, -serverInstance.OriginY);


                var scale = 0.09f;
                pos += new Vector2(-scale * 32 / 2, -scale * 32 / 2);

                var world = Matrix3x2.CreateTranslation(pos);


                dynamic weenieData = new ExpandoObject();

                weenieData.Name = weenie.GetProperty(PropertyString.Name);
                weenieData.Type = ((WeenieType)weenie.Type).ToString();
                weenieData.WCID = weenie.ClassId;


                if ((WeenieType)weenie.Type == WeenieType.HotSpot) {
                    var cloud = MakeRectangle();
                    cloud.Class = "hastooltip";
                    cloud.CustomAttributes = new List<SvgCustomAttribute>() {
                        new SvgCustomAttribute("data-json", HttpUtility.HtmlAttributeEncode(JsonSerializer.Serialize(weenieData)))
                    };
                    cloud.Fill = new SvgPaint(Color.FromArgb(150, 255, 0, 0));
                    cloud.Transform = new List<SvgTransform>() {
                            new SvgMatrixTransform() {
                                Matrix = new GrapeCity.Documents.Common.Matrix(Matrix3x2.CreateTranslation(new Vector2(cell.CellGeometry.EnvCell.Position.Origin.X, -cell.CellGeometry.EnvCell.Position.Origin.Y)))
                            }
                        };
                    cloud.ClipPath = MakeCellClipPath(serverObjsGroup, cell);
                    serverObjsGroup.Children.Insert(0, cloud);
                    return;
                }

                var iconId = weenie.GetProperty(ACE.Entity.Enum.Properties.PropertyDataId.Icon);
                if (!iconId.HasValue)
                    continue;
                var texture = DatManager.PortalDat.ReadFromDat<Texture>(iconId.Value);
                if (texture == null || texture.Id == 0)
                    continue;

                if (!_textureCache.TryGetValue(texture.Id, out SvgElement el)) {
                    var bmp = (IronSoftware.Drawing.AnyBitmap)texture.GetBitmap();
                    var bytes = bmp.ExportBytes(IronSoftware.Drawing.AnyBitmap.ImageFormat.Png);
                    el = new SvgImageElement() {
                        ID = $"0x{texture.Id:X8}",
                        Href = new SvgReference(new Uri($"data:image/bmp;base64,{Convert.ToBase64String(bytes)}")),
                        Transform = new List<SvgTransform>() {
                                new SvgScaleTransform(){
                                    ScaleX = scale,
                                    ScaleY = scale
                                }
                            }
                    };

                    _textureCache.TryAdd(texture.Id, el);
                }

                var use = new SvgUseElement() {
                    Tag = cell.Position.Z,
                    Class = "hastooltip",
                    Href = new SvgReference($"0x{texture.Id:X8}"),
                    Transform = new List<SvgTransform>() {
                            new SvgMatrixTransform() {
                                Matrix = new GrapeCity.Documents.Common.Matrix(world)
                            },
                        },
                    CustomAttributes = new List<SvgCustomAttribute>() {
                        new SvgCustomAttribute("data-guid", serverInstance.Guid.ToString())
                    }
                };

                if (string.IsNullOrWhiteSpace(weenieData.Name)) {
                    weenieData.Name = "Unknown";
                }

                switch ((WeenieType)weenie.Type) {
                    case WeenieType.Creature:
                        weenieData.Level = weenie.GetProperty(PropertyInt.Level);
                        break;
                    case WeenieType.Switch:
                        if (_leverToDoorLookup.TryGetValue(serverInstance.Guid, out var doors)) {
                            foreach (var door in doors) {
                                var targetLbi = _serverInstances.FirstOrDefault(si => si.Guid == door);
                                if (targetLbi == null)
                                    continue;

                                var targetWeenie = EmuData.GetWeenieByClassId(targetLbi.WeenieClassId);
                                if (targetWeenie == null || (WeenieType)targetWeenie.Type != WeenieType.Door)
                                    continue;

                                if (!((IDictionary<String, object>)weenieData).ContainsKey("Doors")) {
                                    weenieData.Doors = new List<ExpandoObject>();
                                }
                                dynamic doorData = new ExpandoObject();

                                doorData.Name = targetWeenie.GetProperty(PropertyString.Name);
                                doorData.Guid = targetLbi.Guid;

                                ((List<ExpandoObject>)weenieData.Doors).Add(doorData);
                            }
                        }
                        break;
                    case WeenieType.Door:
                        var keyCode = weenie.GetProperty(PropertyString.LockCode);
                        // unuseable (lever/door/event or something?)
                        if (weenie.GetProperty(PropertyInt.ItemUseable) == (int)Usable.No) {
                            if (_doorToLeverLookup.TryGetValue(serverInstance.Guid, out var levers)) {
                                foreach (var lever in levers) {
                                    var targetLbi = _serverInstances.FirstOrDefault(si => si.Guid == lever);
                                    if (targetLbi == null)
                                        continue;

                                    var targetWeenie = EmuData.GetWeenieByClassId(targetLbi.WeenieClassId);
                                    if (targetWeenie == null || (WeenieType)targetWeenie.Type != WeenieType.Switch)
                                        continue;

                                    if (!((IDictionary<String, object>)weenieData).ContainsKey("Levers")) {
                                        weenieData.Levers = new List<ExpandoObject>();
                                    }
                                    dynamic leverData = new ExpandoObject();

                                    leverData.Name = targetWeenie.GetProperty(PropertyString.Name);
                                    leverData.Guid = targetLbi.Guid;

                                    ((List<ExpandoObject>)weenieData.Levers).Add(leverData);
                                }
                            }
                        }
                        else {
                            // key locked
                            if (!string.IsNullOrWhiteSpace(keyCode)) {
                                if (keyCode != "masterkey") {
                                    var keyWeenie = EmuData.GetKeyWeenie(keyCode);

                                    if (keyWeenie != null) {
                                        weenieData.NeedsKey = $"{keyWeenie.GetProperty(PropertyString.Name)} (wcid: {keyWeenie.ClassId})";
                                    }
                                }
                            }
                            // locked LP resist
                            if (weenie.GetProperty(PropertyBool.DefaultLocked) == true || weenie.GetProperty(PropertyBool.Locked) == true) {
                                weenieData.LPResist = weenie.GetProperty(PropertyInt.ResistLockpick);
                            }
                        }

                        break;
                }

                var serialized = HttpUtility.HtmlAttributeEncode(JsonSerializer.Serialize(weenieData));
                use.CustomAttributes.Add(new SvgCustomAttribute("data-json", serialized));

                serverObjsGroup.Children.Add(use);
            }
        }

        private SvgElement? RenderStaticObjects(MapChunkGrid chunkGrid) {
            var stabs = chunkGrid.Cells.SelectMany(c => c.CellGeometry.EnvCell.StaticObjects);

            var stabGroup = new SvgGroupElement() {
                Class = "statics"
            };

            foreach (var stab in stabs) {
                var s = RenderStab(stab);
                if (s != null)
                    stabGroup.Children.Add(s);
            }

            return stabGroup.HasChildren ? stabGroup : null;
        }

        private SvgElement? RenderStab(Stab stab) {
            if ((stab.Id & 0x01000000) != 0) {
                var gfxObj = DatManager.PortalDat.ReadFromDat<GfxObj>(stab.Id);
                if (!gfxObj.Flags.HasFlag(GfxObjFlags.HasPhysics)) {
                    return null;
                }
                var pos = Matrix4x4.CreateTranslation(stab.Frame.Origin);
                var rot = Matrix4x4.CreateFromQuaternion(stab.Frame.Orientation);
                return BuildGfxObj(stab.Id, rot * pos);
                /*
                var pos = new Vector2(stab.Frame.Origin.X, stab.Frame.Origin.Y);
                var rot = (float)(QuaternionToHeading(stab.Frame.Orientation));
                var world = Matrix3x2.CreateRotation(rot) * Matrix3x2.CreateTranslation(pos);
                if (!_staticObjectCache.TryGetValue(stab.Id, out var stabGroup)) {
                    stabGroup = BuildGfxObj(stab.Id, Matrix4x4.Identity);
                    stabGroup.ID = $"0x{stab.Id:X8}";
                    if (stabGroup == null) {
                        return null;
                    }

                    _staticObjectCache.Add(stab.Id, stabGroup);
                }

                return new SvgUseElement() {
                    Href = new SvgReference($"0x{stab.Id:X8}"),
                    Transform = new List<SvgTransform>() {
                        new SvgMatrixTransform(){
                            Matrix = new GrapeCity.Documents.Common.Matrix(world)
                        }
                    }
                };
                */
            }
            else if ((stab.Id & 0x02000000) != 0) {
                return BuildStaticObj(stab);
                /*
                if (!_staticObjectCache.TryGetValue(stab.Id, out var stabGroup)) {
                    stabGroup = BuildStaticObj(stab);
                    stabGroup.ID = $"0x{stab.Id:X8}";
                    if (stabGroup == null) {
                        return null;
                    }

                    _staticObjectCache.Add(stab.Id, stabGroup);
                }
                var pos = new Vector2(stab.Frame.Origin.X, -stab.Frame.Origin.Y);
                var rot = (float)(QuaternionToHeading(stab.Frame.Orientation));
                var world = Matrix3x2.CreateRotation(rot) * Matrix3x2.CreateTranslation(pos);

                return new SvgUseElement() {
                    Href = new SvgReference($"0x{stab.Id:X8}"),
                    Transform = new List<SvgTransform>() {
                        new SvgMatrixTransform(){
                            Matrix = new GrapeCity.Documents.Common.Matrix(world)
                        }
                    }
                };
                */
            }

            return null;
        }

        private SvgElement? BuildStaticObj(Stab stab) {
            var stabGroup = new SvgGroupElement() {
                //ID = $"0x{stab.Id:X8}"
            };
            var staticObj = DatManager.PortalDat.ReadFromDat<SetupModel>(stab.Id);
            if (staticObj.Spheres.Count == 0 && staticObj.CylSpheres.Count == 0 && !staticObj.Parts.Any(p => {
                var gfxObj = DatManager.PortalDat.ReadFromDat<GfxObj>(p);
                return gfxObj.Flags.HasFlag(GfxObjFlags.HasPhysics);
            })) return null;

            var partIndexes = Enumerable.Range(0, staticObj.Parts.Count).ToList();

            partIndexes.Sort((a, b) => {
                var az = staticObj.PlacementFrames.Last().Value.AnimFrame.Frames[a].Origin.Z;
                var bz = staticObj.PlacementFrames.Last().Value.AnimFrame.Frames[b].Origin.Z;

                return az.CompareTo(bz);
            });

            var pos = Matrix4x4.CreateTranslation(stab.Frame.Origin);
            var rot = Matrix4x4.CreateFromQuaternion(stab.Frame.Orientation);

            for (var i = 0; i < staticObj.Parts.Count; i++) {
                var frame = staticObj.PlacementFrames.Last().Value.AnimFrame.Frames[partIndexes[i]];
                var pos2 = Matrix4x4.CreateTranslation(frame.Origin);
                var rot2 = Matrix4x4.CreateFromQuaternion(frame.Orientation);

                var s = BuildGfxObj(staticObj.Parts[partIndexes[i]], (rot2 * pos2) * (rot * pos));
                if (s != null)
                    stabGroup.Children.Add(s);
            }

            return stabGroup.HasChildren ? stabGroup : null;
        }

        private SvgElement? BuildGfxObj(uint id, Matrix4x4 world) {
            var gfxObjGroup = new SvgGroupElement() {
                CustomAttributes = new List<SvgCustomAttribute>() {
                        new SvgCustomAttribute("data-gfxobj-id", $"0x{id:X8}")
                    }
            };

            var gfxObj = DatManager.PortalDat.ReadFromDat<GfxObj>(id);

            if (gfxObj.Polygons.Count == 0) {
                return null;
            }

            var polys = gfxObj.Polygons.Values.ToList();
            polys.Sort((a, b) => {
                return gfxObj.VertexArray.Vertices[(ushort)a.VertexIds.First()].Origin.Z.CompareTo(gfxObj.VertexArray.Vertices[(ushort)b.VertexIds.First()].Origin.Z);
            });

            foreach (var p in polys) {
                var pb = new SvgPathBuilder();
                var polyVerts = p.VertexIds.Select(v => {
                    return Vector3.Transform(gfxObj.VertexArray.Vertices[(ushort)v].Origin, world);
                }).ToArray();

                pb.AddMoveTo(false, polyVerts[0].X, -polyVerts[0].Y);
                foreach (var v in polyVerts.Skip(1)) {
                    pb.AddLineTo(false, v.X, -v.Y);
                }
                pb.AddLineTo(false, polyVerts[0].X, -polyVerts[0].Y);

                if (pb.FigureCount > 0) {
                    gfxObjGroup.Children.Add(new SvgPathElement() {
                        PathData = pb.ToPathData()
                    });
                }
            }

            if (!gfxObjGroup.HasChildren)
                return null;
            return gfxObjGroup;
        }

        private SvgElement? RenderChunkGridFloors(MapChunkGrid chunkGrid) {
            var cells = chunkGrid.Cells;

            var x = cells.SelectMany(cell => {
                var pos = new Vector2(cell.Position.X, cell.Position.Y);
                var rot = (float)(QuaternionToHeading(cell.CellGeometry.EnvCell.Position.Orientation));
                var world = Matrix3x2.CreateRotation(rot) * Matrix3x2.CreateTranslation(pos);

                return cell.CellGeometry.WalkablePolys.Select(fp => {
                    return new Clipper2Lib.PathD(fp.Select(v => {
                        var t = Vector2.Transform(new Vector2(v.X, v.Y), world);
                        return new Clipper2Lib.PointD(t.X, t.Y);
                    }).ToList());
                }).ToList();
            });
            var subject = new Clipper2Lib.PathsD(x);
            var paths = Clipper2Lib.Clipper.Union(subject, new Clipper2Lib.PathsD(), Clipper2Lib.FillRule.Positive, 2);

            var pb = new SvgPathBuilder();
            foreach (var path in paths) {
                pb.AddMoveTo(false, (float)path[0].x, (float)-path[0].y);
                for (var i = 1; i < path.Count; i++) {
                    pb.AddLineTo(false, (float)path[i].x, (float)-path[i].y);
                }
                pb.AddLineTo(false, (float)path[0].x, (float)-path[0].y);
            }

            var pathData = pb.ToPathData();

            var isCatwalk = chunkGrid.Cells.All(c => c.CellGeometry.WalkablePolys.Count == 0 || c.CellGeometry.IsCatwalk);

            return (pb.FigureCount > 0) ? new SvgPathElement() {
                Class = isCatwalk ? "catwalk" : "walkable",
                PathData = pathData
            } : null;
        }

        private SvgElement? RenderChunkGridWalls(MapChunkGrid chunkGrid) {
            var pb = new SvgPathBuilder();
            foreach (var cell in chunkGrid.Cells) {
                if (cell.CellGeometry.WalkablePolys.Count == 0 || cell.CellGeometry.IsCatwalk)
                    continue;
                var pos = new Vector2(cell.Position.X, cell.Position.Y);
                var rot = (float)(QuaternionToHeading(cell.CellGeometry.EnvCell.Position.Orientation));
                var world = Matrix3x2.CreateRotation(rot) * Matrix3x2.CreateTranslation(pos);

                foreach (var path in cell.CellGeometry.WallLines) {
                    if (path.Count == 0)
                        continue;
                    var lines = path.Select(v => Vector2.Transform(new Vector2(v.X, v.Y), world))
                        .Select(v => new Vector2((float)Math.Round(v.X, 2), -(float)Math.Round(v.Y, 2))).ToArray();
                    pb.AddMoveTo(false, lines[0].X, lines[0].Y);
                    for (var i = 1; i < lines.Length; i++) {
                        pb.AddLineTo(false, lines[i].X, lines[i].Y);
                    }
                    pb.AddLineTo(false, lines[0].X, lines[0].Y);
                }
            }

            return (pb.FigureCount > 0) ? new SvgPathElement() {
                Class = "wall",
                PathData = pb.ToPathData()
            } : null;
        }

        double QuaternionToHeading(Quaternion q) {
            double sinr = 2 * (q.W * q.Z + q.X * q.Y);
            double cosr = 1 - 2 * (q.Y * q.Y + q.Z * q.Z);
            return Math.Atan2(sinr, cosr);
        }

        public string ToSVGString() {
            var sb = new StringBuilder();
            svg.Save(sb, new System.Xml.XmlWriterSettings() {
                Indent = true,
            });
            return sb.ToString()
                .Replace(" href=", " xlink:href=")
                .Replace(" xmlns=\"http://www.w3.org/2000/svg\"", " xmlns=\"http://www.w3.org/2000/svg\"  xmlns:xlink=\"http://www.w3.org/1999/xlink\"")
                .Replace("</defs>", $"{BuildFilters()}</defs>"); ;
        }

        private SvgElement MakeGridPattern(string name) {
            MakePatternGroup();

            var gridPattern = new SvgPatternElement() {
                ID = $"grid-pattern-{name}",
                Width = new SvgLength(10),
                Height = new SvgLength(10),
                X = new SvgLength(5),
                Y = new SvgLength(5),
                PatternContentUnits = SvgUnitType.UserSpaceOnUse,
                PatternUnits = SvgUnitType.UserSpaceOnUse
            };

            gridPattern.Children.Add(new SvgUseElement() {
                Href = new SvgReference(_patternGroup.ID)
            });

            return gridPattern;
        }

        private void MakePatternGroup() {
            if (_patternGroup != null)
                return;

            _patternGroup = new SvgGroupElement() {
                ID = "grid-pattern-contents"
            };

            var rectPath = new SvgPathBuilder();
            rectPath.AddMoveTo(false, 0, 0);
            rectPath.AddLineTo(false, 0, 10);
            rectPath.AddLineTo(false, 10, 10);
            rectPath.AddLineTo(false, 10, 0);
            rectPath.AddLineTo(false, 0, 0);

            _patternGroup.Children.Add(new SvgPathElement() {
                PathData = rectPath.ToPathData()
            });

            for (var i = 0; i <= 10; i++) {
                _patternGroup.Children.Add(new SvgLineElement() {
                    X1 = new SvgLength(0),
                    X2 = new SvgLength(10),
                    Y1 = new SvgLength(i),
                    Y2 = new SvgLength(i)
                });
                _patternGroup.Children.Add(new SvgLineElement() {
                    X1 = new SvgLength(i),
                    X2 = new SvgLength(i),
                    Y1 = new SvgLength(0),
                    Y2 = new SvgLength(10)
                });
            }
        }

        private string BuildFilters() {
            return @"
  <filter id=""catwalk-shadow"">
    <feDropShadow dx=""0"" dy=""0"" stdDeviation=""1"" flood-color=""rgb(20,20,20)"" />
  </filter>
    <filter id=""spotlight"">
      <feFlood
        result=""floodFill""
        x=""0""
        y=""0""
        width=""100%""
        height=""100%""
        flood-color=""green""
        flood-opacity=""0.4"" />
      <feBlend in=""SourceGraphic"" in2=""floodFill"" mode=""multiply"" />
    </filter>
  ";
        }

        private string BuildStyles() {
            var styles = new StringBuilder();
            styles.AppendLine(@"
    .wall {
        stroke: #111111;
        stroke-width: 0.22;
        stroke-linejoin: butt;
    }
    .catwalk {
        fill: #f7f7d2;
        opacity: 0.6;
        stroke: #111;
        stroke-width: 0.11;
        stroke-linejoin: butt;
        filter: url(#catwalk-shadow);
    }
    .statics {
        fill: #aaa;
        opacity: 0.8;
        stroke: #444;
        stroke-width: 0.08;
        stroke-linejoin: butt;
    }
    .cell-portal {
        stroke: red;
        stroke-width: 0.5;
        stroke-linejoin: round;
    }

    #grid-pattern-contents path {
        stroke: #000000;
        stroke-width: 0.2;
        stroke-opacity: 0.1;
    }
    #grid-pattern-contents line {
        stroke: #000000;
        stroke-width: 0.1;
        stroke-opacity: 0.08;
    }
  ");
            var colors = new Dictionary<int, Color>() {
                { 0, Color.FromArgb(0, 139, 163) },
                { 40, Color.FromArgb(220, 147, 11) },
                { 60, Color.FromArgb(255, 210, 0) },
                { 100, Color.FromArgb(167, 167, 167) },
            };

            var x = ImmutableList.CreateRange(colors.Keys.Select(c => {
                var color = new GradientColorByValue(colors[c], c);
                return color;
            }));
            var gradientColorProvider = new GradientColorProvider(x);

            var f = 5;

            for (var i = 0; i < FloorChunkCount; i++) {
                var p = 50 + ((i - (FloorChunkCount / 2)) * f);
                var o = (int)((float)i * 100f / (float)(FloorChunkCount -1));
                var color = gradientColorProvider.PickColor(p);

                styles.AppendLine($"[data-z-rel=\"{i}\"] .walkable {{ fill: url(#grid-pattern-{i}); }}");
                styles.AppendLine($"#grid-pattern-{i} {{ fill:#{(color.ToArgb() & 0x00FFFFFF):X6}; }}");
            }
            return styles.ToString();
        }
    }
}
