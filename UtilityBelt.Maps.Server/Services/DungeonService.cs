﻿using System.Text.Json;
using UtilityBelt.Maps.Server.Models;
using UtilityBelt.Navigation.Data;
using UtilityBelt.Navigation.Geometry.Maps;

namespace UtilityBelt.Maps.Server.Services {
    public class DungeonService {
        public DungeonInfo[] GetDungeonInfos() {
            return Dungeons.GetDungeonInfos();
        }

        public DungeonMap? GetDungeonMap(uint landcellId) {
            var generator = new DungeonMapGenerator(landcellId, this);

            return generator.GetDungeonMap();
        }
    }
}
