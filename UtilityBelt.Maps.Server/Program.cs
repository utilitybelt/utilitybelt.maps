using ACE.DatLoader;
using Microsoft.AspNetCore.ResponseCompression;
using System.Net;
using UtilityBelt.Maps;
using UtilityBelt.Maps.Server.Lib;
using UtilityBelt.Maps.Server.Services;
using UtilityBelt.Navigation.Data;

namespace ACInspector.Web {
    public class Program {
        public static void Main(string[] args) {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllersWithViews();
            builder.Services.AddRazorPages();

            builder.Services.AddControllers().AddJsonOptions(o => {
                o.JsonSerializerOptions.Converters.Add(new Vector2JsonConverter());
                o.JsonSerializerOptions.Converters.Add(new Vector3JsonConverter());
                o.JsonSerializerOptions.Converters.Add(new QuaternionJsonConverter());
                o.JsonSerializerOptions.WriteIndented = true;
            });
            builder.Services.AddSingleton<DungeonService>();

            builder.Services.AddMvc((options) => {
                options.ReturnHttpNotAcceptable = false;
            });

            builder.WebHost.ConfigureKestrel((options, serverOptions) => {
                serverOptions.Listen(IPAddress.Any, 5000);
            });


            var datPath = System.Environment.GetEnvironmentVariable("AC_DAT_PATH");
            if (string.IsNullOrWhiteSpace(datPath)) {
                datPath = @"C:\Turbine\Asheron's Call\";
            }
            Console.WriteLine($"Using Dat Directory: {datPath}");

            DatManager.Initialize(datPath);



            var dbPath = System.Environment.GetEnvironmentVariable("AC_DB_PATH");
            if (string.IsNullOrWhiteSpace(dbPath)) {
                dbPath = @"C:\Turbine\Asheron's Call\ace_world.db";
            }
            Console.WriteLine($"Using SQLite Database: {dbPath}");
            EmuData.Init(dbPath);


            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }
            else {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();
            app.MapRazorPages();
            app.MapControllers();
            app.MapFallbackToFile("index.html");

            app.Run();
        }
    }
}