﻿using System.Globalization;
using System.Numerics;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace UtilityBelt.Maps.Server.Lib {
    public class Vector3JsonConverter : JsonConverter<Vector3> {
        public override Vector3 Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
            return Vector3.Zero;
        }

        public override void Write(Utf8JsonWriter writer, Vector3 v3, JsonSerializerOptions options) {
            writer.WriteStartArray();
            writer.WriteNumberValue(v3.X);
            writer.WriteNumberValue(v3.Y);
            writer.WriteNumberValue(v3.Z);
            writer.WriteEndArray();
        }
    }

    public class Vector2JsonConverter : JsonConverter<Vector2> {
        public override Vector2 Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
            return Vector2.Zero;
        }

        public override void Write(Utf8JsonWriter writer, Vector2 v2, JsonSerializerOptions options) {
            writer.WriteStartArray();
            writer.WriteNumberValue(v2.X);
            writer.WriteNumberValue(v2.Y);
            writer.WriteEndArray();
        }
    }

    public class QuaternionJsonConverter : JsonConverter<Quaternion> {
        public override Quaternion Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
            return Quaternion.Identity;
        }

        public override void Write(Utf8JsonWriter writer, Quaternion q, JsonSerializerOptions options) {
            writer.WriteStartArray();
            writer.WriteNumberValue(q.X);
            writer.WriteNumberValue(q.Y);
            writer.WriteNumberValue(q.Z);
            writer.WriteNumberValue(q.W);
            writer.WriteEndArray();
        }
    }
}
