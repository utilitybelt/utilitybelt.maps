﻿using System.Numerics;

namespace UtilityBelt.Maps.Server.Models {
    public class DungeonMapCell {
        public uint Id { get; set; }
        public ushort CellStruct { get; set; }
        public uint EnvironmentId { get; set; }
        public Vector3 Origin { get; internal set; }
        public Quaternion Orientation { get; internal set; }

        public DungeonMapCell(uint id) {
            Id = id;
        }
    }
}