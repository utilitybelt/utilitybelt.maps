﻿namespace UtilityBelt.Maps.Server.Models {
    public class DungeonMap {
        public uint Id { get; set; }
        public string Name { get; set; } = "Unknown";

        public Dictionary<uint, MapStaticObject> Statics { get; set; } = new Dictionary<uint, MapStaticObject>();
        public List<MapInstanceObject> Instances { get; set; } = new List<MapInstanceObject>();
        public Dictionary<uint, DungeonMapEnvironment> Environments { get; set; } = new Dictionary<uint, DungeonMapEnvironment>();
        public List<DungeonMapChunk> Chunks { get; set; } = new List<DungeonMapChunk>();
    }
}
