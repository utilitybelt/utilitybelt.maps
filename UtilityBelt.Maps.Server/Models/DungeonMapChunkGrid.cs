﻿namespace UtilityBelt.Maps.Server.Models {
    public class DungeonMapChunkGrid {
        public List<DungeonMapCell> Cells { get; set; } = new List<DungeonMapCell>();

        public DungeonMapChunkGrid() {

        }
    }
}
