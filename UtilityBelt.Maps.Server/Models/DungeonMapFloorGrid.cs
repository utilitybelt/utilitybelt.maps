﻿namespace UtilityBelt.Maps.Server.Models {
    public class DungeonMapFloorGrid {
        public Dictionary<int, DungeonMapChunkGrid> ChunkByFloorGrids { get; set; } = new Dictionary<int, DungeonMapChunkGrid>();

        public DungeonMapFloorGrid() {
        
        }

        public DungeonMapChunkGrid? AtFloorLevel(int level) {
            ChunkByFloorGrids.TryGetValue(level, out DungeonMapChunkGrid? chunkGrid);
            return chunkGrid;
        }
    }
}
