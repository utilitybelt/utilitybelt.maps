﻿namespace UtilityBelt.Maps.Server.Models {
    public class DungeonMapChunk {
        public string Name { get; set; } = "Unknown";
        public Dictionary<uint, DungeonMapCell> Cells { get; set; } = new Dictionary<uint, DungeonMapCell>();
    }
}