﻿using System.Numerics;

namespace UtilityBelt.Maps.Server.Models {
    public class DungeonMapEnvironmentStructure {
        public List<List<float>> FloorPolys { get; set; } = new List<List<float>>();
        public List<List<float>> WallLines { get; set; } = new List<List<float>>();
    }
}