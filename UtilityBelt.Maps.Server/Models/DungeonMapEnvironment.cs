﻿namespace UtilityBelt.Maps.Server.Models {
    public class DungeonMapEnvironment {
        public Dictionary<ushort, DungeonMapEnvironmentStructure> Structures { get; set; } = new Dictionary<ushort, DungeonMapEnvironmentStructure>();

        public DungeonMapEnvironment() {
            
        }
    }
}
