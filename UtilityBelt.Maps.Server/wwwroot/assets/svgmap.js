let pixCanvas;
let pixCanvas1;

function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;
    return txt.value;
}

class SVGMap {
    #didCombine = false;
    #isSettingUp = true;
    #needsInitialChunkSetup = true;
    #stepTime = 111
    #startChunkCount = 0
    lockedChunkIds = []
    #grid = undefined

    constructor(svgElement) {
        this.svg = svgElement;

        this.#setupArrows();

        const floorChunks = this.svg.getElementsByClassName("floor-chunk");
        this.chunkIdLookup = {}

        const $this = this;

        this.chunks = Array.prototype.map.call(floorChunks, (chunkEl) => {
            const chunk = new Chunk($this, chunkEl);
            this.chunkIdLookup[chunk.id] = chunk;
            return chunk;
        })

        this.#startChunkCount = this.chunks.length

        this.#setup();
    }

    update() {
        this.#updateArrows();
        const bb = this.svg.getBBox()
        this.svg.setAttribute("viewBox", `${bb.x - 1} ${bb.y - 1} ${bb.width + 2} ${bb.height + 2}`);

        if (this.#isSettingUp)
            return;

        this.chunks.forEach((chunk) => {
            chunk.el.style["stroke"] = "";
        });

        for (let i = 0; i < this.chunks.length; i++) {
            const chunk = this.chunks[i]
            d3.select(chunk.el).attr("transform", `translate(${chunk.x}, ${chunk.y})`)
            for (let k = i + 1; k < this.chunks.length; k++) {
                if (this.chunks[i].overlaps(this.chunks[k])) {
                    this.chunks[i].el.style["stroke"] = "red";
                    this.chunks[k].el.style["stroke"] = "red";
                }
            }
        }

        this.#grid.tick();
    }

    #setup() {
        if (this.#ensureSVGsLoaded(this.#stepTime)) {
            return setTimeout(() => { this.#setup() }, this.#stepTime);
        }

        if (!this.#didCombine) {
            const chunkCombineStepTime = Math.max(this.#stepTime - Math.min(this.#startChunkCount * this.#stepTime / 50, this.#stepTime), 50)
            if (this.#tryCombineChunks(chunkCombineStepTime)) {
                return setTimeout(() => { this.#setup() }, chunkCombineStepTime);
            }
            this.#didCombine = true;
        }

        if (this.#makeInitialGridLayout(this.#stepTime)) {
            return setTimeout(() => { this.#setup() }, this.#stepTime);
        }

        if (this.#needsInitialChunkSetup) {
            this.chunks.sort((a, b) => b.cellCount - a.cellCount);
            this.chunks.forEach((chunk) => {
                chunk.setupDragDrop();
            })

            this.lockedChunkIds = [ this.chunks[0].id ]

            this.#needsInitialChunkSetup = false;
        }

        this.#isSettingUp = false;

        this.update()
    }

    #makeInitialGridLayout(stepTime) {
        if (this.#grid == undefined) {
            const nodes = this.chunks
            const usedConnections = [];
            const links = [];

            for (const chunk of this.chunks) {
                const bb = chunk.el.getBBox();
                chunk.radius = Math.sqrt(bb.width + bb.height)
                for (let i = 0; i < chunk.connections.length; i++) {
                    const connection = chunk.connections[i];
                    const otherChunk = this.chunkIdLookup[connection.endId];

                    if (usedConnections.indexOf(connection.key) != -1) {
                        continue;
                    }
                    usedConnections.push(connection.key);

                    links.push({
                        source: chunk.id,
                        target: otherChunk.id
                    });
                }
            }

            const $this = this;
            this.#grid = d3.forceSimulation(nodes)
                .force("link", d3.forceLink(links).id(d => d.id).strength(1))
                .force("charge", d3.forceManyBody().strength(-150))
                .force("x", d3.forceX())
                .force("y", d3.forceY())
                .force("collide", d3.forceCollide().radius(d => d.radius * 2).iterations(2))
                .on("tick", () => {
                    $this.update()
                });
        }


        //if (this.#grid == undefined) {
            //this.#grid = new AnnealingGridLayout(this);
        //}

        //this.#grid.update();
        this.update()

        return false;
    }

    #tryCombineChunks(stepTime) {
        const $this = this;
        for (let i = 0; i < this.chunks.length; i++) {
            const chunk = this.chunks[i];
            for (let k = 0; k < chunk.connections.length; k++) {
                const otherChunk = this.chunkIdLookup[chunk.connections[k].endId];
                if (!chunk.overlaps(otherChunk)) {
                    $this.#mergeChunks(chunk, otherChunk)
                    d3.select(chunk.el)
                        .selectAll('.walkable')
                        .transition().duration(0)
                        .style("stroke-width", "0")
                        .style("stroke", "rgb(0, 0 ,0)")
                        .transition().duration(stepTime / 2)
                        .style("stroke-width", "2")
                        .style("stroke", "rgb(0, 255 ,0)")
                        .transition().duration(stepTime / 2)
                        .style("stroke-width", "0")
                        .style("stroke", "rgb(0, 0 ,0)")
                        .on('end', () => {
                            d3.select(chunk.el)
                                .selectAll('.walkable')
                                .style("stroke-width", "")
                                .style("stroke", "")
                        });
                    return true;
                }
            }
        }

        return false;
    }

    #mergeChunks(firstChunk, secondChunk) {
        var g = this.svg.getRootNode().createElementNS('http://www.w3.org/2000/svg', 'g');
        g.setAttribute("class", "chunk-container");

        const parent = firstChunk.el.parentElement;

        if (firstChunk.isContainer()) {
            while (firstChunk.el.children.length > 0) {
                g.appendChild(firstChunk.el.children[0]);
            }
            firstChunk.el.remove();
        }
        else {
            g.appendChild(firstChunk.el);
        }
        
        if (secondChunk.isContainer()) {
            while (secondChunk.el.children.length > 0) {
                g.appendChild(secondChunk.el.children[0]);
            }
            secondChunk.el.remove();
        }
        else {
            g.appendChild(secondChunk.el);
        }

        const index = this.chunks.indexOf(secondChunk);
        if (index != -1) {
            this.chunks.splice(index, 1);
        }

        firstChunk.connections = firstChunk.connections.filter((connection) => connection.endId != secondChunk.id);
        secondChunk.connections = secondChunk.connections.filter((connection) => connection.endId != firstChunk.id);
        secondChunk.connections.forEach((con) => {
            const otherChunk = this.chunkIdLookup[con.endId];
            otherChunk.connections.forEach((otherCon) => {
                if (otherCon.endId == secondChunk.id) {
                    otherCon.endId = firstChunk.id;
                }
            })
        })
        firstChunk.connections = firstChunk.connections.concat(secondChunk.connections.map((con) => {
            con.startId = firstChunk.id;
            return con;
        }))

        firstChunk.el = g;
        parent.appendChild(g);
        firstChunk.update();
    }

    #ensureSVGsLoaded() {
        for (let i = 0; i < this.chunks.length; i++) {
            const chunk = this.chunks[i];
            if (!(chunk.img.complete && chunk.img.naturalHeight !== 0)) {
                return true;
            }
        }

        return false;
    }

    #setupArrows() {
        const arrowPoints = [[0, 0], [0, 20], [20, 10]];

        d3.select(this.svg.querySelector("defs"))
            .append('marker')
            .attr('id', 'arrow')
            .attr('viewBox', [0, 0, 20, 20])
            .attr('refX', 15)
            .attr('refY', 10)
            .attr('markerWidth', 5)
            .attr('markerHeight', 5)
            .attr('orient', 'auto-start-reverse')
            .append('path')
            .attr('d', d3.line()(arrowPoints))
            .attr('stroke', '#666')
            .attr('fill', '#666');

        this.arrows = d3.select(this.svg)
            .append("g")
            .attr("class", "arrows")
    }

    #updateArrows() {
        d3.selectAll(this.svg.getElementsByClassName("arrow")).remove();

        // keep track of drawn connections, so we only draw each once.
        const usedConnections = []

        for (const chunk of this.chunks) {
            for (let i = 0; i < chunk.connections.length; i++) {
                const connection = chunk.connections[i];
                const otherChunk = this.chunkIdLookup[connection.endId];

                if (usedConnections.indexOf(connection.key) != -1) {
                    continue;
                }
                usedConnections.push(connection.key);

                const x1 = chunk.x + connection.startX;
                const x2 = otherChunk.x + connection.endX;
                const y1 = chunk.y + connection.startY;
                const y2 = otherChunk.y + connection.endY;

                if (Math.abs(chunk.x - otherChunk.x) + Math.abs(chunk.y - otherChunk.y) <= 1)
                    continue;

                this.arrows
                    .append('path')
                    .attr('class', 'arrow')
                    .attr('d', d3.line()([[x1, y1], [x2, y2]]))
                    .attr('stroke', '#0005')
                    .attr('stroke-width', '0.3')
                    .attr('stroke-dasharray', '1 0.5')
                    .attr('marker-start', 'url(#arrow)')
                    .attr('marker-end', 'url(#arrow)')
                    .attr('fill', 'none');
            }
        }
    }
}

class AnnealingGridLayout {
    #map = undefined
    #size = 0
    #chunks = []
    #maxChunkWidth = 0
    #maxChunkHeight = 0

    #maxTemperature = 1
    #minTemperature = 0.001
    #currentTemperature = 0
    #lastState = false
    #lastEnergy = false
    #bestState = false
    #bestEnergy = false

    constructor(map) {
        this.#map = map
        this.#size = map.chunks.length;

        
        for (let i = 0; i < this.#map.chunks.length; i++) {
            const chunk = this.#map.chunks[i];
            chunk.update();
            const bb = chunk.el.getBBox();
            if (bb.width > this.#maxChunkWidth) {
                this.#maxChunkWidth = bb.width;
            }
            if (bb.height > this.#maxChunkHeight) {
                this.#maxChunkHeight = bb.height;
            }
        }

        for (let x = 0; x < this.#size; x++) {
            this.#chunks[x] = []
            for (let y = 0; y < this.#size; y++) {
                this.#chunks[x].push(false)
            }
        }

        this.#drawGrid();
        this.#organizeRandomly();

        this.#currentTemperature = this.#maxTemperature
        this.#lastState = this.#chunks.map((x) => x.map((y) => y))
        this.#lastEnergy = 1000000000000; // this.#getEnergy(this.#lastState)
        this.#bestState = this.#lastState
        this.#bestEnergy = this.#lastEnergy
    }



    #drawGrid() {
        const root = d3.select(this.#map.svg)
            .append("g")
            .attr("class", "grid")
        for (let x = 0; x < this.#size - 1; x++) {
            root.append('line')
                .style("stroke", "lightblue")
                .style("stroke-width", 1)
                .attr("x1", (x + 1) * this.#maxChunkWidth)
                .attr("x2", (x + 1) * this.#maxChunkWidth)
                .attr("y1", 0)
                .attr("y2", this.#size * this.#maxChunkHeight);
            root.append('line')
                .style("stroke", "lightblue")
                .style("stroke-width", 1)
                .attr("x1", 0)
                .attr("x2", this.#size * this.#maxChunkWidth)
                .attr("y1", (x + 1) * this.#maxChunkHeight)
                .attr("y2", (x + 1) * this.#maxChunkHeight);
        }
    }

    #organizeRandomly() {
        let placed = 0;
        while (placed < this.#chunks.length) {
            const x = Math.round(Math.random() * (this.#size - 1))
            const y = Math.round(Math.random() * (this.#size - 1))

            if (this.#chunks[x][y] == false) {
                this.#chunks[x][y] = this.#map.chunks[placed++]
            }
        }
    }

    #getEnergy(state) {
        let energy = 0.0;

        // keep track of drawn connections, so we only draw each once.
        const usedConnections = []

        for (const chunk of this.#map.chunks) {
            for (let i = 0; i < chunk.connections.length; i++) {
                const connection = chunk.connections[i];
                const otherChunk = this.#map.chunkIdLookup[connection.endId];

                if (usedConnections.indexOf(connection.key) != -1) {
                    continue;
                }
                usedConnections.push(connection.key);

                const x1 = chunk.x + connection.startX;
                const x2 = otherChunk.x + connection.endX;
                const y1 = chunk.y + connection.startY;
                const y2 = otherChunk.y + connection.endY;

                energy += Math.abs(Math.sqrt(Math.pow(Math.abs(x2 - x1), 2)) + Math.pow(Math.abs(y2 - y1), 2))
            }
        }

        return energy / 100000;
    }

    #newState(lastState) {
        const newState = lastState.map((x) => x.map((y) => y))
        const chunkToMove = this.#map.chunks[Math.round((this.#map.chunks.length - 1) * Math.random())]
        const dir = Math.round(Math.random() * 7)
        let direction = [0, 0]
        switch (dir) {
            case 0: direction = [-1, -1]; break;
            case 1: direction = [0, -1]; break;
            case 2: direction = [1, -1]; break;
            case 3: direction = [1, 0]; break;
            case 4: direction = [1, 1]; break;
            case 5: direction = [0, 1]; break;
            case 6: direction = [-1, 1]; break;
            case 7: direction = [-1, 0]; break;
        }


        for (let x = 0; x < this.#size; x++) {
            for (let y = 0; y < this.#size; y++) {
                const chunk = newState[x][y]
                if (chunk != chunkToMove)
                    continue;

                if (Math.random() < 0.11) {
                    direction[0] *= Math.round(Math.random() * 3)
                    direction[1] *= Math.round(Math.random() * 3)
                }

                let nx = direction[0] + x;
                let ny = direction[1] + y;

                if (nx < 0) nx = x;
                if (nx > this.#size - 1) nx = x;
                if (ny < 0) ny = y;
                if (ny > this.#size - 1) y;

                const otherChunk = newState[nx][ny];

                if (nx == x && ny == y || otherChunk == undefined)
                    continue;
                newState[nx][ny] = chunk;
                newState[x][y] = otherChunk;

                const mx = (nx * this.#maxChunkWidth) + (this.#maxChunkWidth - chunk.el.getBBox().width) / 2
                const my = (ny * this.#maxChunkHeight) + (this.#maxChunkHeight - chunk.el.getBBox().height) / 2
                newState[nx][ny].move(mx, my);

                if (otherChunk != false) {
                    const nmx = (x * this.#maxChunkWidth) + (this.#maxChunkWidth - otherChunk.el.getBBox().width) / 2
                    const nmy = (y * this.#maxChunkHeight) + (this.#maxChunkHeight - otherChunk.el.getBBox().height) / 2
                    newState[x][y].move(nmx, nmy);
                }
                break;
            }
        }

        return newState
    }

    update() {
        if (this.#currentTemperature > this.#minTemperature) {
            let currentState = false;
            let currentEnergy = 0;
            for (var i = 0; i < 1; i++) {
                currentState = this.#newState(this.#lastState);
                currentEnergy = this.#getEnergy(currentState);

                if (currentEnergy < this.#lastEnergy) {
                    this.#lastState = currentState;
                    this.#lastEnergy = currentEnergy;
                } else {
                    if (Math.random() <= this.#currentTemperature / 10) {
                        this.#lastState = currentState;
                        this.#lastEnergy = currentEnergy;
                    }
                }
            }

            if (this.#bestEnergy >= this.#lastEnergy) {
                this.#bestState = this.#lastState;
                this.#bestEnergy = this.#lastEnergy; 
            }
            this.#currentTemperature *= 0.9

            console.log("Energy:", this.#lastEnergy, " / ", currentEnergy, "Temp:", this.#currentTemperature);
        }

        for (let x = 0; x < this.#size; x++) {
            for (let y = 0; y < this.#size; y++) {
                const chunk = this.#lastState[x][y]
                if (chunk == false)
                    continue;
                const mx = (x * this.#maxChunkWidth) + (this.#maxChunkWidth - chunk.el.getBBox().width) / 2
                const my = (y * this.#maxChunkHeight) + (this.#maxChunkHeight - chunk.el.getBBox().height) / 2 
                chunk.move(mx, my);
            }
        }
    }
}

class Chunk {
    constructor(map, chunkElement) {
        this.map = map;
        this.el = chunkElement;
        this.id = parseInt(this.el.attributes["data-chunk-id"].textContent);

        this.img = new Image();
        this.x = 0;
        this.y = 0;
        this.sx = 0;
        this.sy = 0;
        this.px = 0;
        this.py = 0;
        this.cellCount = 0;

        this.connections = []
        this.#buildInitialConnections()
        this.update();
    }

    update() {
        this.#updateCellCount();
        this.#setupCanvas();
    }

    isContainer() {
        return this.el.attributes['class'] && this.el.attributes['class'].textContent.indexOf("chunk-container") != -1
    }

    pushWithChildren(connection) {
        const otherChunk = this.map.chunkIdLookup[connection.endId];
        const newX = otherChunk.x + connection.endX - connection.startX;
        const newY = otherChunk.y + connection.endY - connection.startY;
        otherChunk.pushRecursive(newX, newY, []);
    }

    pushRecursive(x, y, pushed) {
        if (pushed.indexOf(this.id) != -1)
            return;

        const $this = this;
        d3.select(this.el)
            .attr("transform", `translate(${this.x}, ${this.y})`)
            .transition().duration(this.map.stepTime / 2)
            .attr("transform", `translate(${x}, ${y})`)
            .on('end', () => {
                $this.x = x;
                $this.y = y;
            });

        pushed.push(this.id);

        this.connections.forEach((connection) => {
            if ($this.map.lockedChunkIds.indexOf(connection.endId) != -1)
                return;
            const connectedChunk = $this.map.chunkIdLookup[connection.endId];
            connectedChunk.px = x;
            connectedChunk.py = y;
            connectedChunk.pushRecursive(x, y, pushed);
        })
    }

    push(x, y) {
        this.x = this.px + x;
        this.y = this.py + y;
        d3.select(this.el)
            .transition().duration(this.map.stepTime / 4)
            .attr("transform", `translate(${this.x}, ${this.y})`)
    }

    move(x, y) {
        d3.select(this.el)
            .transition().duration(this.map.stepTime / 4)
            .attr("transform", `translate(${x}, ${y})`)
    }

    #updateCellCount() {
        this.cellCount = 0;
        const walkableNodes = this.el.querySelectorAll('.walkable');
        for (let i = 0; i < walkableNodes.length; i++) {
            this.cellCount += walkableNodes[i].parentElement.attributes['data-cells'].textContent.split(',').length;
        }
    }

    #setupCanvas() {
        this.sx = undefined;
        this.sy = undefined;
        const walkableNodes = this.el.querySelectorAll('.walkable');
        this.map.svg.querySelectorAll('.instances, .statics, .wall').forEach((el) => el.style['display'] = 'none');
        const mapBB = this.map.svg.querySelector('.root').getBBox();
        this.map.svg.querySelectorAll('.instances, .statics, .wall').forEach((el) => el.style['display'] = '');
        for (let i = 0; i < walkableNodes.length; i++) {
            const walkableNodeBB = walkableNodes[i].getBBox();
            const x = walkableNodeBB.x - mapBB.x;
            const y = walkableNodeBB.y - mapBB.y;
            if (this.sx == undefined || x < this.sx) {
                this.sx = x;
            }
            if (this.sy == undefined || y < this.sy) {
                this.sy = y;
            }
        }

        var URL = window.URL || window.webkitURL || window;
        var svgNS = 'http://www.w3.org/2000/svg';
        this.el.querySelectorAll('.instances, .statics, .wall').forEach((el) => el.style['display'] = 'none');
        var bb = this.el.getBBox()
        this.el.querySelectorAll('.instances, .statics, .wall').forEach((el) => el.style['display'] = '');
        var svgEl = document.createElementNS(svgNS, "svg")
        svgEl.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns', svgNS)
        svgEl.setAttributeNS(svgNS, "width", bb.width)
        svgEl.setAttributeNS(svgNS, "height", bb.height)
        var g = document.createElementNS(svgNS, "g");
        g.setAttributeNS(svgNS, "transform", `translate(${-bb.x}, ${-bb.y})`);
        svgEl.appendChild(g);

        for (var i = 0; i < walkableNodes.length; i++) {
            g.appendChild(walkableNodes[i].cloneNode(true))
        }

        //document.getElementById("preview").parentElement.appendChild(svgEl);
        //svgEl.style["position"] = "absolute";
        //svgEl.style["z-index"] = "2000";

        const url = URL.createObjectURL(new Blob(['<?xml version="1.0" encoding="utf-8"?>' + svgEl.outerHTML], { type: 'image/svg+xml;charset=utf-8' }));

        const $this = this;
        this.img.onload = function () {
            $this.map.update();
            URL.revokeObjectURL(url);
        }
        this.img.src = url;
    }

    setupDragDrop() {
        const $this = this;

        let currentX = 0;
        let currentY = 0;

        d3.select(this.el).call(d3.drag()
            .on("start", (event, d) => {
                currentX = event.x;
                currentY = event.y;
                d3.select($this.el).raise().attr("stroke-width", "0.6").attr("stroke", "black");
            })
            .on("drag", (event, d) => {
                $this.x = $this.x + event.x - currentX;
                $this.y = $this.y + event.y - currentY;

                d3.select($this.el).attr("transform", `translate(${$this.x}, ${$this.y})`)
                currentX = event.x;
                currentY = event.y;

                $this.map.update();
            })
            .on("end", (event, d) => {
                d3.select($this.el).attr("stroke", null);
            }));
    }

    // returns true if any pixels are overlapping
    // https://stackoverflow.com/questions/40952985/how-to-perform-per-pixel-collision-test-for-transparent-images
    overlaps(otherChunk) {
        var slowButPerfect = false;
        // img1, x, y, w, h, img2, x1, y1, w1, h1
        var img1 = this.img;
        var x = this.sx + this.x;
        var y = this.sy + this.y;
        var w = this.img.width;
        var h = this.img.height;
        var img2 = otherChunk.img;
        var x1 = otherChunk.sx + otherChunk.x;
        var y1 = otherChunk.sy + otherChunk.y;
        var w1 = otherChunk.img.width;
        var h1 = otherChunk.img.height;
        var ax, aw, ay, ah, ctx, ctx1, i;
        // function to check if any pixels are visible
        function checkPixels(context, w, h) {
            try {
                var imageData = new Uint32Array(context.getImageData(0, 0, w, h).data.buffer);
                var i = 0;
                // if any pixel is not zero then there must be an overlap
                while (i < imageData.length) {
                    if (imageData[i++] > 30) {
                        return true;
                    }
                }
            }
            catch { }
            return false;

        }

        // check if they overlap
        if (x >= x1 + w1 || x + w <= x1 || y >= y1 + h1 || y + h <= y1) {
            return false; // no overlap 
        }
        // size of overlapping area
        // find left edge
        ax = x < x1 ? x1 : x;
        // find right edge calculate width
        aw = x + w < x1 + w1 ? (x + w) - ax : (x1 + w1) - ax
        // do the same for top and bottom
        ay = y < y1 ? y1 : y;
        ah = y + h < y1 + h1 ? (y + h) - ay : (y1 + h1) - ay

        // Create a canvas to do the masking on
        if (pixCanvas === undefined) {
            pixCanvas = document.createElement("canvas");
        }
        pixCanvas.width = aw;
        pixCanvas.height = ah;
        ctx = pixCanvas.getContext("2d");

        // draw the first image relative to the overlap area
        ctx.drawImage(img1, x - ax, y - ay);

        // set the composite operation to destination-in
        ctx.globalCompositeOperation = "destination-in"; // this means only pixels
        // will remain if both images
        // are not transparent
        ctx.drawImage(img2, x1 - ax, y1 - ay);
        ctx.globalCompositeOperation = "source-over";

        // are we using slow method???
        if (slowButPerfect) {
            return checkPixels(ctx, aw, ah);
        }

        // now draw over its self to amplify any pixels that have low alpha
        //for (var i = 0; i < 32; i++) {
        //    ctx.drawImage(pixCanvas, 0, 0);
        //}
        // create a second canvas 1/8th the size but not smaller than 1 by 1
        if (pixCanvas1 === undefined) {
            pixCanvas1 = document.createElement("canvas");
        }
        ctx1 = pixCanvas1.getContext("2d");
        // reduced size rw, rh
        var rw = pixCanvas1.width = Math.max(1, Math.floor(aw / 8));
        var rh = pixCanvas1.height = Math.max(1, Math.floor(ah / 8));
        
        // repeat the following untill the canvas is just 64 pixels
        while (rw > 8 && rh > 8) {
            // draw the mask image several times

            for (i = 0; i < 32; i++) {
                ctx1.drawImage(
                    pixCanvas,
                    0, 0, aw, ah,
                    Math.random(),
                    Math.random(),
                    rw, rh
                );
            }
            // clear original
            ctx.clearRect(0, 0, aw, ah);
            // set the new size
            aw = rw;
            ah = rh;
            // draw the small copy onto original
            ctx.drawImage(pixCanvas1, 0, 0);
            // clear reduction canvas
            ctx1.clearRect(0, 0, pixCanvas1.width, pixCanvas1.height);
            // get next size down
            rw = Math.max(1, Math.floor(rw / 8));
            rh = Math.max(1, Math.floor(rh / 8));
        }
        
        // check for overlap
        return checkPixels(ctx, aw, ah);
    }

    #buildInitialConnections() {
        if (!this.el.attributes["data-connections"] || this.el.attributes["data-connections"].textContent.length == 0)
            return;

        const connections = JSON.parse(decodeHtml(this.el.attributes["data-connections"].textContent));

        this.connections = connections.map((connection) => {
            return new Connection(this.id, connection.X1, connection.Y1, connection.OtherChunkId, connection.X2, connection.Y2)
        })
    }
}

class Connection {
    constructor(startId, startX, startY, endId, endX, endY) {
        this.startId = startId;
        this.startX = startX;
        this.startY = startY;
        this.endId = endId;
        this.endX = endX;
        this.endY = endY;

        if (startId < endId) {
            this.key = `${startId}.${endId}.${startX}.${startY}`
        }
        else {
            this.key = `${endId}.${startId}.${endX}.${endY}`
        }

    }
}