﻿using ACE.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using UtilityBelt.Navigation.Geometry.Maps;
using UtilityBelt.Navigation.Data;
using UtilityBelt.Maps;
using ACE.Database.Models.World;
using GrapeCity.Documents.Svg;
using System.Numerics;
using UtilityBelt.Maps.Server.Models;
using UtilityBelt.Maps.Server.Services;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ACInspector.Web.Server.Controllers {
    [ApiController]
    [Route("api/v1/[controller]")]
    public class MapsController : ControllerBase {
        private readonly ILogger<MapsController> _logger;
        private readonly DungeonService _dungeonService;

        public MapsController(ILogger<MapsController> logger, DungeonService dungeonService) {
            _logger = logger;
            _dungeonService = dungeonService;
        }

        [HttpGet]
        public IEnumerable<DungeonInfo> GetAll() {
            var dungeonInfos = _dungeonService.GetDungeonInfos().ToList();

            dungeonInfos.Sort((a, b) => a.Name.CompareTo(b.Name));

            return dungeonInfos;
        }

        [Route("{landblockId}")]
        public DungeonMap? GetLandblockGeometry(string landblockId) {
            if (!Dungeons.TryParseLandblockId(landblockId, out uint landcellId)) {
                return null;
            }

            return _dungeonService.GetDungeonMap(landcellId);
        }

        [Route("min/{landblockId}.svg")]
        public IActionResult GetLandblockChunkTinyImage(string landblockId) {
            if (!Dungeons.TryParseLandblockId(landblockId, out uint landcellId)) {
                return null;
            }

            var map = new MapLandblockGeometry(landcellId & 0xFFFF0000, landcellId & 0xFFFF);
            var builder = new SVGBuilder(map);

            return Content(builder.ToSVGString(), "image/svg+xml");
        }

        [Route("{landblockId}.{format}")]
        public IActionResult GetLandblockChunkImage(string landblockId, string format) {
            format = format.ToLower();

            var mapSvgOptions = new MapSVGOptions() {
                ShowStaticObjects = Request.Query["statics"].FirstOrDefault()?.ToLower() != "false",
                ShowWalls = Request.Query["walls"].FirstOrDefault()?.ToLower() != "false",
                ShowFloors = Request.Query["floors"].FirstOrDefault()?.ToLower() != "false",
            };

            var showInstances = Request.Query["instances"].FirstOrDefault()?.ToLower() ?? "icons";
            switch (showInstances) {
                case "polys":
                    mapSvgOptions.ShowServerInstances = true;
                    mapSvgOptions.UseInstanceIcons = false;
                    break;
                case "none":
                    mapSvgOptions.ShowServerInstances = false;
                    break;
                default:
                    mapSvgOptions.ShowServerInstances = true;
                    mapSvgOptions.UseInstanceIcons = true;
                    break;
            }

            if (!string.IsNullOrWhiteSpace(Request.Query["bg"].FirstOrDefault())) {
                mapSvgOptions.BackgroundFill = Request.Query["bg"].FirstOrDefault();
            }

            if (!Dungeons.TryParseLandblockId(landblockId, out uint landcellId)) {
                Response.StatusCode = 400;
                return Content($"Unable to parse landblock id: {landblockId}. Must be an integer in hex or decimal format.");
            }

            var mapGeometry = new MapLandblockGeometry(landcellId & 0xFFFF0000, landcellId & 0x0000FFFF);

            switch (format) {
                case "svg":
                    var svgBuilder = new MapSVGBuilder(mapGeometry, mapSvgOptions);
                    return Content(svgBuilder.ToSVGString(), "image/svg+xml");
                case "png":
                default:
                    Response.StatusCode = 400;
                    return Content($"Invalid file format: {format}. Must be one of: svg.");
            }
        }
    }
}