﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace ACE {
    internal static class  NetStandardNeededExtensions {
        #region Queue<T>
        public static bool TryPeek<T>(this Queue<T> set, out T val) {
            if (set.Count == 0) {
                val = default(T);
                return false;
            }

            val = set.Peek();

            return true;
        }
        #endregion

        #region Queue<T>
        public static bool TryAdd<T>(this HashSet<T> set, T val) {
            if (set.Contains(val)) {
                return false;
            }

            return set.Add(val);
        }
        public static bool TryGetValue<T>(this HashSet<T> set, T val, out T found) {
            if (set.Contains(val)) {
                found = val;
                return true;
            }

            found = default(T);
            return false;
        }
        #endregion

        #region IEnumerable<T>
        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> enumerable) {
            var set = new HashSet<T>();
            foreach (var item in enumerable) {
                set.TryAdd(item);
            }
            return set;
        }
        #endregion

        #region Dictionary<TKey, TVal>
        public static bool TryAdd<TKey, TVal>(this Dictionary<TKey, TVal> dictionary, TKey key, TVal val) {
            if (dictionary.ContainsKey(key)) {
                return false;
            }

            dictionary.Add(key, val);

            return true;
        }

        public static bool Remove<TKey, TVal>(this Dictionary<TKey, TVal> dictionary, TKey key, out TVal val) {
            if (!dictionary.ContainsKey(key)) {
                val = default(TVal);
                return false;
            }

            val = dictionary[key];
            return true;
        }
        #endregion // Dictionary<TKey, TVal>

        #region Math Extensions

        // Licensed to the .NET Foundation under one or more agreements.
        // The .NET Foundation licenses this file to you under the MIT license.

        // ===================================================================================================
        // Portions of the code implemented below are based on the 'Berkeley SoftFloat Release 3e' algorithms.
        // ===================================================================================================


        private static void ThrowMinMaxException<T>(T min, T max) {
            throw new ArgumentException();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static byte Clamp(byte value, byte min, byte max) {
            if (min > max) {
                ThrowMinMaxException(min, max);
            }

            if (value < min) {
                return min;
            }
            else if (value > max) {
                return max;
            }

            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static decimal Clamp(decimal value, decimal min, decimal max) {
            if (min > max) {
                ThrowMinMaxException(min, max);
            }

            if (value < min) {
                return min;
            }
            else if (value > max) {
                return max;
            }

            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static double Clamp(double value, double min, double max) {
            if (min > max) {
                ThrowMinMaxException(min, max);
            }

            if (value < min) {
                return min;
            }
            else if (value > max) {
                return max;
            }

            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static short Clamp(short value, short min, short max) {
            if (min > max) {
                ThrowMinMaxException(min, max);
            }

            if (value < min) {
                return min;
            }
            else if (value > max) {
                return max;
            }

            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int Clamp(int value, int min, int max) {
            if (min > max) {
                ThrowMinMaxException(min, max);
            }

            if (value < min) {
                return min;
            }
            else if (value > max) {
                return max;
            }

            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static long Clamp(long value, long min, long max) {
            if (min > max) {
                ThrowMinMaxException(min, max);
            }

            if (value < min) {
                return min;
            }
            else if (value > max) {
                return max;
            }

            return value;
        }

        /// <summary>Returns <paramref name="value" /> clamped to the inclusive range of <paramref name="min" /> and <paramref name="max" />.</summary>
        /// <param name="value">The value to be clamped.</param>
        /// <param name="min">The lower bound of the result.</param>
        /// <param name="max">The upper bound of the result.</param>
        /// <returns>
        ///   <paramref name="value" /> if <paramref name="min" /> ≤ <paramref name="value" /> ≤ <paramref name="max" />.
        ///
        ///   -or-
        ///
        ///   <paramref name="min" /> if <paramref name="value" /> &lt; <paramref name="min" />.
        ///
        ///   -or-
        ///
        ///   <paramref name="max" /> if <paramref name="max" /> &lt; <paramref name="value" />.
        /// </returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static nint Clamp(nint value, nint min, nint max) {
            if (min > max) {
                ThrowMinMaxException(min, max);
            }

            if (value < min) {
                return min;
            }
            else if (value > max) {
                return max;
            }

            return value;
        }

        [CLSCompliant(false)]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static sbyte Clamp(sbyte value, sbyte min, sbyte max) {
            if (min > max) {
                ThrowMinMaxException(min, max);
            }

            if (value < min) {
                return min;
            }
            else if (value > max) {
                return max;
            }

            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float Clamp(float value, float min, float max) {
            if (min > max) {
                ThrowMinMaxException(min, max);
            }

            if (value < min) {
                return min;
            }
            else if (value > max) {
                return max;
            }

            return value;
        }

        [CLSCompliant(false)]
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static ushort Clamp(ushort value, ushort min, ushort max) {
            if (min > max) {
                ThrowMinMaxException(min, max);
            }

            if (value < min) {
                return min;
            }
            else if (value > max) {
                return max;
            }

            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [CLSCompliant(false)]
        public static uint Clamp(uint value, uint min, uint max) {
            if (min > max) {
                ThrowMinMaxException(min, max);
            }

            if (value < min) {
                return min;
            }
            else if (value > max) {
                return max;
            }

            return value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [CLSCompliant(false)]
        public static ulong Clamp(ulong value, ulong min, ulong max) {
            if (min > max) {
                ThrowMinMaxException(min, max);
            }

            if (value < min) {
                return min;
            }
            else if (value > max) {
                return max;
            }

            return value;
        }

        /// <summary>Returns <paramref name="value" /> clamped to the inclusive range of <paramref name="min" /> and <paramref name="max" />.</summary>
        /// <param name="value">The value to be clamped.</param>
        /// <param name="min">The lower bound of the result.</param>
        /// <param name="max">The upper bound of the result.</param>
        /// <returns>
        ///   <paramref name="value" /> if <paramref name="min" /> ≤ <paramref name="value" /> ≤ <paramref name="max" />.
        ///
        ///   -or-
        ///
        ///   <paramref name="min" /> if <paramref name="value" /> &lt; <paramref name="min" />.
        ///
        ///   -or-
        ///
        ///   <paramref name="max" /> if <paramref name="max" /> &lt; <paramref name="value" />.
        /// </returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        [CLSCompliant(false)]
        public static nuint Clamp(nuint value, nuint min, nuint max) {
            if (min > max) {
                ThrowMinMaxException(min, max);
            }

            if (value < min) {
                return min;
            }
            else if (value > max) {
                return max;
            }

            return value;
        }
        #endregion // Math Extensions
    }
}
