﻿using ACE.DatLoader;
using Newtonsoft.Json;
using UtilityBelt.Maps;
using UtilityBelt.Maps.Server.Services;
using UtilityBelt.Navigation.Geometry.Maps;

namespace MapExporter {
    internal class Program {
        public static CellDatDatabase CellDat => DatManager.CellDat;
        public static PortalDatDatabase PortalDat => DatManager.PortalDat;

        static void Main(string[] args) {
            var settings = new JsonSerializerSettings() {
                TypeNameHandling = TypeNameHandling.None,
                //TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
                //ContractResolver = new NonPublicPropertiesResolver()
            };

            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            var datPath = @"C:\Turbine\Asheron's Call\";
            var dbPath = @"C:\Turbine\Asheron's Call\ace_world.db";

            Console.WriteLine($"Using SQLite Database: {dbPath}");
            Console.WriteLine($"Using Dats: {datPath}");

            DatManager.Initialize(datPath, true, true);
            EmuData.Init(dbPath);

            if (Directory.Exists("data")) {
                Directory.Delete("data", true);
            }

            Directory.CreateDirectory("data");

            DumpAllDungeonMaps();
        }

        public static void DumpAllDungeonMaps() {
            for (uint i = 0; i <= 0xFFFF; i++) {
                try {
                    var lbId = i << 16;

                    var geometry = new MapLandblockGeometry(lbId);

                    if (geometry.LandblockGeometry.HasDungeons() && geometry.DungeonChunks.Count() > 0) {
                        foreach (var chunk in geometry.DungeonChunks) {
                            var cells = chunk.Cells.Select(c => c.CellGeometry.CellId & 0x0000FFFF).ToList();
                            cells.Sort();
                            var id = lbId + (cells.First() & 0x0000FFFF);
                            Console.WriteLine($"Dumping dungeon svg: 0x{id:X8}");
                            var geometry2 = new MapLandblockGeometry(lbId, chunk.Cells.First().CellGeometry.CellId & 0x0000FFFF);
                            var builder = new SVGBuilder(geometry2);

                            File.WriteAllText($"data/0x{id:X8}.svg", builder.ToSVGString());
                        }
                    }
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}