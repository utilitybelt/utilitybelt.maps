#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 5000

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

WORKDIR /src
COPY ["UtilityBelt.Maps.Server/UtilityBelt.Maps.Server.csproj", "UtilityBelt.Maps.Server/"]
COPY ["ACE/ACE.Database/ACE.Database.csproj", "ACE/ACE.Database/"]
COPY ["ACE/ACE.Entity/ACE.Entity.csproj", "ACE/ACE.Entity/"]
COPY ["ACE/ACE.Common/ACE.Common.csproj", "ACE/ACE.Common/"]
COPY ["UtilityBelt.Maps/UtilityBelt.Maps.csproj", "UtilityBelt.Maps/"]
COPY ["ACE/ACE.DatLoader/ACE.DatLoader.csproj", "ACE/ACE.DatLoader/"]
COPY ["UtilityBelt.Navigation/UtilityBelt.Navigation.csproj", "UtilityBelt.Navigation/"]
COPY ["ACE/ACE.Physics/ACE.Physics.csproj", "ACE/ACE.Physics/"]
RUN dotnet restore "UtilityBelt.Maps.Server/UtilityBelt.Maps.Server.csproj"
COPY . .
WORKDIR "/src/UtilityBelt.Maps.Server"
RUN dotnet build "UtilityBelt.Maps.Server.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "UtilityBelt.Maps.Server.csproj" -c Release -o /app/publish /p:UseAppHost=false

FROM base AS final
RUN apt-get update
RUN apt-get install -y libfontconfig1 fontconfig
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "UtilityBelt.Maps.Server.dll"]