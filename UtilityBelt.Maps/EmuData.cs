﻿using ACE.Database;
using ACE.Database.Models.World;
using ACE.Entity.Enum.Properties;
using ACE.Entity.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ACE.Entity;

namespace UtilityBelt.Maps {
    public static class EmuData {
        public static WorldDbContext DBContext { get; private set; }
        public static WorldDatabaseWithEntityCache WorldDB { get; private set; }

        private static Dictionary<uint, Weenie> _weenieCache = new Dictionary<uint, Weenie>();

        public static void Init(string dbPath) {
            ACE.Common.ConfigManager.Initialize(new ACE.Common.MasterConfiguration() {
                MySql = new ACE.Common.DatabaseConfiguration() {
                    World = new ACE.Common.MySqlConfiguration() {
                        ConnectionString = $"Data Source={dbPath}"
                    }
                },
                Server = new ACE.Common.GameConfiguration(),
                Offline = new ACE.Common.OfflineConfiguration()
            });

            DBContext = new WorldDbContext();
            WorldDB = new WorldDatabaseWithEntityCache();
        }

        public static List<LandblockInstance> GetServerObjects(uint landblockId) {
            return WorldDB.GetCachedInstancesByLandblock(DBContext, (ushort)(landblockId >> 16));
        }

        public static Weenie GetWeenieByClassId(uint weenieClassId) {
            if (!_weenieCache.TryGetValue(weenieClassId, out Weenie weenie)) {
                weenie = WorldDB.GetWeenie(weenieClassId);
                _weenieCache.TryAdd(weenieClassId, weenie);
            }
            return weenie;
        }

        public static Weenie GetKeyWeenie(string keyCode) {
            var query = from weenieRecord in DBContext.Weenie
                        join strings in DBContext.WeeniePropertiesString on weenieRecord.ClassId equals strings.ObjectId
                        where weenieRecord.Type == (int)WeenieType.Key && strings.Type == (ushort)PropertyString
                        .KeyCode && strings.Value == keyCode
                        select weenieRecord;

            var w = query.FirstOrDefault();

            return w == null ? null : GetWeenieByClassId(w.ClassId);
        }
    }
}
