﻿using GrapeCity.Documents.Svg;
using System.Numerics;
using System.Text;
using System.Linq;
using GrapeCity.Documents.Drawing;
using ACE.DatLoader;
using ACE.DatLoader.FileTypes;
using System.IO;
using UtilityBelt.Navigation.Geometry.Maps;
using System;
using System.Collections.Generic;
using ACE.Database.Models.World;
using ACE.Entity.Enum;
using System.Web;
using System.Runtime.InteropServices;

namespace UtilityBelt.Maps {
    public class MapSVGOptions {
        public bool ShowStaticObjects { get; set; } = true;
        public bool ShowWalls { get; set; } = true;
        public bool ShowFloors { get; set; } = true;
        public bool ShowGrid { get; set; } = true;
        public bool ShowServerInstances { get; set; } = true;
        public bool UseInstanceIcons { get; set; } = true;
        public string BackgroundFill { get; set; } = "transparent";
    }

    public class MapSVGBuilder : IDisposable {
        private GcSvgDocument svg;
        private Dictionary<uint, SvgElement> _textureCache = new Dictionary<uint, SvgElement>();
        private Dictionary<uint, SvgGroupElement> _staticCache = new Dictionary<uint, SvgGroupElement>();
        private Dictionary<string, SvgGroupElement> _environmentCache = new Dictionary<string, SvgGroupElement>();
        private List<LandblockInstance> _serverInstances = new List<LandblockInstance>();

        public MapLandblockGeometry MapGeometry { get; }
        public MapSVGOptions Options { get; }

        public MapSVGBuilder(MapLandblockGeometry mapGeometry, MapSVGOptions options) {
            MapGeometry = mapGeometry;
            svg = new GcSvgDocument();

            Options = options ?? new MapSVGOptions();

            Load();
            Build();
        }

        private void Load() {
            if (Options.ShowServerInstances) {
                _serverInstances = EmuData.GetServerObjects(MapGeometry.LandblockId);
            }
        }

        private void Build() {
            var minX = float.MaxValue;
            var maxX = float.MinValue;
            var minY = float.MaxValue;
            var maxY = float.MinValue;

            foreach (var chunk in MapGeometry.DungeonChunks) {
                foreach (var cell in chunk.Cells) {
                    if ((cell.CellGeometry.EnvCell.Position.Origin.X - 5f) < minX) minX = (cell.CellGeometry.EnvCell.Position.Origin.X - 5f);
                    if ((cell.CellGeometry.EnvCell.Position.Origin.Y - 5f) < minY) minY = (cell.CellGeometry.EnvCell.Position.Origin.Y - 5f);
                    if ((cell.CellGeometry.EnvCell.Position.Origin.X + 5f) > maxX) maxX = (cell.CellGeometry.EnvCell.Position.Origin.X + 5f);
                    if ((cell.CellGeometry.EnvCell.Position.Origin.Y + 5f) > maxY) maxY = (cell.CellGeometry.EnvCell.Position.Origin.Y + 5f);
                }
            }

            svg.RootSvg.ShapeRendering = SvgShapeRendering.CrispEdges;
            svg.RootSvg.ViewBox = new SvgViewBox(minX -2f, -maxY - 2f, maxX - minX + 4f, maxY - minY + 4f);

            var style = new SvgStyleElement();
            style.Children.Add(new SvgContentElement(System.Xml.XmlNodeType.Text) {
                Content = BuildStyles()
            });

            svg.RootSvg.Children.Add(style);

            var dungeonGroup = new SvgGroupElement() {
                ID = $"Landblock-0x{MapGeometry.LandblockId:X8}",
                Class = "root",
            };
            foreach (var chunk in MapGeometry.DungeonChunks) {
                var dungeonChunkGroup = new SvgGroupElement() {
                    Class = "dungeon-chunk"
                };

                var lastFloorZ = float.MinValue;
                SvgGroupElement currentFloorChunkGroup = null;

                var floorChunks = chunk.FloorChunks.ToList();
                floorChunks.Sort((a, b) => {
                    var az = a.Cells.First().Position.Z;
                    var bz = b.Cells.First().Position.Z;
                    if (a.Cells.First().CellGeometry.IsCatwalk)
                        az += 2f;
                    if (b.Cells.First().CellGeometry.IsCatwalk)
                        bz += 2f;

                    return az.CompareTo(bz);
                });

                foreach (var floorChunk in floorChunks) {
                    var floorGroup = BuildFloorChunk(floorChunk, out float floorZ);

                    if (currentFloorChunkGroup == null || (Math.Abs(lastFloorZ - floorZ) > 1f && Math.Abs(floorZ - float.MinValue) > 1f)) {
                        if (currentFloorChunkGroup != null && currentFloorChunkGroup.HasChildren) {
                            dungeonChunkGroup.Children.Add(currentFloorChunkGroup);
                        }
                        currentFloorChunkGroup = new SvgGroupElement() {
                            Class = "floor-level",
                            CustomAttributes = new List<SvgCustomAttribute>() {
                                new SvgCustomAttribute("data-floor-z", floorZ.ToString())
                            }
                        };
                    }
                    if (floorGroup.HasChildren) {
                        lastFloorZ = floorZ;
                        currentFloorChunkGroup?.Children.Insert(0, floorGroup);
                    }
                }
                if (currentFloorChunkGroup != null && currentFloorChunkGroup.HasChildren) {
                    dungeonChunkGroup.Children.Add(currentFloorChunkGroup);
                }
                dungeonGroup.Children.Add(dungeonChunkGroup);
            }
            svg.RootSvg.Children.Add(dungeonGroup);

            var defs = new SvgDefsElement();

            defs.Children.Add(MakeGridPattern("0"));
            defs.Children.Add(MakeGridPattern("catwalk"));

            var staticGroup = new SvgGroupElement() {
                ID = "statics"
            };
            staticGroup.Children.AddRange(_staticCache.Values);
            if (staticGroup.Children.Count > 0)
                defs.Children.Add(staticGroup);

            var envGroup = new SvgGroupElement() {
                ID = "environments"
            };
            envGroup.Children.AddRange(_environmentCache.Values);
            if (envGroup.Children.Count > 0)
                defs.Children.Add(envGroup);

            var texturesGroups = new SvgGroupElement() {
                ID = "textures"
            };
            texturesGroups.Children.AddRange(_textureCache.Values);
            if (texturesGroups.Children.Count > 0)
                defs.Children.Add(texturesGroups);

            svg.RootSvg.Children.Insert(1, defs);
        }

        private string BuildStyles() {
            return @"
    svg {
        background-color: " + Options.BackgroundFill + @"
    }
    .floor {
        fill: url(#grid-pattern-0);
    }
    .floor-chunk.catwalk {
        filter: url(#ramp-shadow)
    }
    .catwalk {
        fill: url(#grid-pattern-catwalk);
    }
    .wall {
        stroke: #111111;
        stroke-width: 2;
        vector-effect: non-scaling-stroke;
    }

    .static-objects {
        stroke-width: 1;
        fill: #feafff;
        stroke: #d083ff;
    }

    .server-instances {
        stroke-width: 1;
        fill: #f00;
        stroke: #a00;
    }

    .static {
        vector-effect: non-scaling-stroke;
    }

    use[data-type=""Portal""] {
        stroke-width: 8;
        stroke: #f0f;
        stroke-linecap: round;
        opacity: 0.9;
    }

    use[data-type=""HotSpot""] {
        fill: #0f0;
        opacity: 0.6;
        stroke-width: 0;
    }

    #grid-pattern-catwalk path {
        fill: #f0ca97;
        stroke: #b37017;
        stroke-width: 2;
        vector-effect: non-scaling-stroke;
        stroke-opacity: 0.25;
        fill-opacity: 0.5;
    }

    #grid-pattern-catwalk line {
        stroke: #965e14;
        stroke-width: 0.15;
        stroke-opacity: 0.08;
    }

    #grid-pattern-0 path {
        fill: #ffa021;
        stroke: #b37017;
        stroke-width: 2;
        vector-effect: non-scaling-stroke;
        stroke-opacity: 0.25;
    }

    #grid-pattern-0 line {
        stroke: #965e14;
        stroke-width: 1;
        vector-effect: non-scaling-stroke;
        stroke-opacity: 0.08;
    }
  ";
        }

        private SvgElement MakeGridPattern(string name) {
            var gridPattern = new SvgPatternElement() {
                ID = $"grid-pattern-{name}",
                Width = new SvgLength(10),
                Height = new SvgLength(10),
                X = new SvgLength(5),
                Y = new SvgLength(5),
                PatternContentUnits = SvgUnitType.UserSpaceOnUse,
                PatternUnits = SvgUnitType.UserSpaceOnUse
            };

            var rectPath = new SvgPathBuilder();
            rectPath.AddMoveTo(false, 0, 0);
            rectPath.AddLineTo(false, 0, 10);
            rectPath.AddLineTo(false, 10, 10);
            rectPath.AddLineTo(false, 10, 0);
            rectPath.AddLineTo(false, 0, 0);

            gridPattern.Children.Add(new SvgPathElement() {
                PathData = rectPath.ToPathData()
            });

            for (var i = 0; i <= 10; i++) {
                gridPattern.Children.Add(new SvgLineElement() {
                    X1 = new SvgLength(0),
                    X2 = new SvgLength(10),
                    Y1 = new SvgLength(i),
                    Y2 = new SvgLength(i)
                });
                gridPattern.Children.Add(new SvgLineElement() {
                    X1 = new SvgLength(i),
                    X2 = new SvgLength(i),
                    Y1 = new SvgLength(0),
                    Y2 = new SvgLength(10)
                });
            }

            return gridPattern;
        }

        private SvgGroupElement BuildFloorChunk(MapCellGeometryContainer floorChunk, out float z) {
            z = float.MinValue;
            var chunkGroup = new SvgGroupElement() {
                Class = "floor-chunk"
            };

            if (floorChunk.Cells.All(c => c.CellGeometry.IsCatwalk)) {
                chunkGroup.Class = "floor-chunk catwalk";
            }

            var envCellsGroup = new SvgGroupElement() {
                Class = "envcells"
            };

            var staticsGroup = new SvgGroupElement() {
                Class = "static-objects"
            };

            var serverObjsGroup = new SvgGroupElement() {
                Class = "server-instances"
            };

            foreach (var cell in floorChunk.Cells) {
                if (cell.CellGeometry.EnvCell.EnvironmentId == 0x0D0000CA || cell.CellGeometry.EnvCell.EnvironmentId == 0x0D00016D) {
                    continue;
                }
                var cellGroup = BuildCell(cell);
                if (cellGroup != null) {
                    if (cellGroup.Tag != null && cellGroup.Tag is float x) {
                        envCellsGroup.Tag = x;
                    }
                    envCellsGroup.Children.Add(cellGroup);
                }

                if (Options.ShowStaticObjects) {
                    BuildStaticObjects(staticsGroup, cell);
                }
                if (Options.ShowServerInstances) {
                    BuildServerObjs(serverObjsGroup, cell);
                }
            }

            if (envCellsGroup.HasChildren) {
                if (envCellsGroup.Tag != null && envCellsGroup.Tag is float x) {
                    z = x;
                }
                chunkGroup.Children.Add(envCellsGroup);
            }
            if (staticsGroup.HasChildren) {
                if (staticsGroup.Tag != null && staticsGroup.Tag is float x) {
                    z = x;
                }
                chunkGroup.Children.Add(staticsGroup);
            }
            if (serverObjsGroup.HasChildren) {
                if (staticsGroup.Tag != null && staticsGroup.Tag is float x) {
                    z = x;
                }
                chunkGroup.Children.Add(serverObjsGroup);
            }

            return chunkGroup;
        }

        private void BuildServerObjs(SvgGroupElement serverObjsGroup, MapCellGeometry cell) {
            if (!(System.Environment.OSVersion.Platform is PlatformID.Win32NT or PlatformID.Win32Windows))
                AppContext.SetSwitch("System.Drawing.EnableUnixSupport", true);
            foreach (var serverInstance in _serverInstances.Where(si => si.ObjCellId == cell.CellGeometry.CellId)) {
                var weenie = EmuData.GetWeenieByClassId(serverInstance.WeenieClassId);

                if (weenie == null)
                    continue;

                var pos = new Vector2(serverInstance.OriginX, -serverInstance.OriginY);
                var rot = (float)(QuaternionToHeading(new Quaternion(serverInstance.AnglesX, serverInstance.AnglesY, serverInstance.AnglesZ, serverInstance.AnglesW)));
                var scale = 0.09f;
                pos += new Vector2(-scale * 32/2, -scale * 32/2);

                var world = Matrix3x2.CreateTranslation(pos);

                if (Options.UseInstanceIcons) {
                    var iconId = weenie.GetProperty(ACE.Entity.Enum.Properties.PropertyDataId.Icon);
                    if (!iconId.HasValue)
                        continue;
                    var texture = DatManager.PortalDat.ReadFromDat<Texture>(iconId.Value);
                    if (texture == null || texture.Id == 0)
                        continue;

                    if (!_textureCache.TryGetValue(texture.Id, out SvgElement el)) {
                        var bmp = (IronSoftware.Drawing.AnyBitmap)texture.GetBitmap();
                        var bytes = bmp.ExportBytes(IronSoftware.Drawing.AnyBitmap.ImageFormat.Png);
                        el = new SvgImageElement() {
                            ID = $"0x{texture.Id:X8}",
                            Href = new SvgReference(new Uri($"data:image/bmp;base64,{Convert.ToBase64String(bytes)}")),
                            Transform = new List<SvgTransform>() {
                                new SvgScaleTransform(){
                                    ScaleX = scale,
                                    ScaleY = scale
                                }
                            }
                        };

                        _textureCache.TryAdd(texture.Id, el);
                    }

                    var use = new SvgUseElement() {
                        Tag = cell.Position.Z,
                        Href = new SvgReference($"0x{texture.Id:X8}"),
                        Transform = new List<SvgTransform>() {
                            new SvgMatrixTransform() {
                                Matrix = new GrapeCity.Documents.Common.Matrix(world)
                            },
                        },
                        CustomAttributes = new List<SvgCustomAttribute>() {
                            new SvgCustomAttribute("data-cell-id", $"0x{cell.CellGeometry.EnvCell.Id:X8}"),
                            new SvgCustomAttribute("data-wcid", weenie.ClassId.ToString()),
                            new SvgCustomAttribute("data-type", $"{(WeenieType)weenie.Type}"),
                            new SvgCustomAttribute("data-icon", $"0x{iconId:X8}")
                        }
                    };

                    var name = weenie.GetProperty(ACE.Entity.Enum.Properties.PropertyString.Name);
                    if (!string.IsNullOrEmpty(name)) {
                        use.CustomAttributes.Add(new SvgCustomAttribute("data-name", name));
                    }

                    serverObjsGroup.Children.Add(use);
                }
                else {
                    var setupId = weenie.GetProperty(ACE.Entity.Enum.Properties.PropertyDataId.Setup);
                    if (setupId.HasValue) {
                        var s = BuildStab(serverObjsGroup, cell, setupId.Value);
                        if (s != null) {
                            if (s.CustomAttributes == null)
                                s.CustomAttributes = new List<SvgCustomAttribute>();

                            s.CustomAttributes.Add(new SvgCustomAttribute("data-wcid", weenie.ClassId.ToString()));
                            s.CustomAttributes.Add(new SvgCustomAttribute("data-type", $"{(WeenieType)weenie.Type}"));

                            var name = weenie.GetProperty(ACE.Entity.Enum.Properties.PropertyString.Name);
                            if (!string.IsNullOrEmpty(name)) {
                                s.CustomAttributes.Add(new SvgCustomAttribute("data-name", name));
                            }

                            serverObjsGroup.Tag = cell.Position.Z;
                            s.Transform = new List<SvgTransform>() {
                                new SvgMatrixTransform() {
                                    Matrix = new GrapeCity.Documents.Common.Matrix(world)
                                }
                            };
                            serverObjsGroup.Children.Add(s);
                        }
                    }
                }
            }
        }

        private SvgElement BuildCell(MapCellGeometry cell) {
            var key = $"0x{cell.CellGeometry.EnvCell.EnvironmentId:X8}-{cell.CellGeometry.EnvCell.CellStructure}";
            if (!_environmentCache.TryGetValue(key, out SvgGroupElement envGroup)) {
                envGroup = new SvgGroupElement() {
                    ID = key,
                    Tag = cell.Position.Z
                };

                if (Options.ShowFloors) {
                    BuildFloors(envGroup, cell);
                }

                if (Options.ShowWalls && !cell.CellGeometry.IsCatwalk) {
                    BuildWalls(envGroup, cell);
                }

                if (!envGroup.HasChildren)
                    return null;

                _environmentCache.Add(key, envGroup);
            }

            var pos = new Vector2(cell.CellGeometry.EnvCell.Position.Origin.X, -cell.CellGeometry.EnvCell.Position.Origin.Y);
            var rot = (float)(QuaternionToHeading(cell.CellGeometry.EnvCell.Position.Orientation));
            var world = Matrix3x2.CreateRotation(rot) * Matrix3x2.CreateTranslation(pos);
            return new SvgUseElement() {
                Tag = cell.Position.Z,
                Href = new SvgReference(key),
                Transform = new List<SvgTransform>() {
                        new SvgMatrixTransform() {
                            Matrix = new GrapeCity.Documents.Common.Matrix(world)
                        },
                    },
                CustomAttributes = new List<SvgCustomAttribute>() {
                        new SvgCustomAttribute("data-cell-id", $"0x{cell.CellGeometry.EnvCell.Id:X8}")
                    }
            };
        }

        private void BuildFloors(SvgGroupElement floorsGroup, MapCellGeometry cell) {
            var cellStructKey = cell.CellGeometry.EnvCell.CellStructure;
            if (!cell.CellGeometry.Environment.Cells.ContainsKey(cellStructKey))
                return;

            var polys = cell.CellGeometry.Environment.Cells[cellStructKey].PhysicsPolygons;
            var vertices = cell.CellGeometry.Environment.Cells[cellStructKey].VertexArray.Vertices;
            if (polys.Count == 0) {
                return;
            }
            float walkableThr = (float)Math.Cos(50 / 180.0f * Math.PI);

            List<List<Vector3>> tris = new List<List<Vector3>>();
            foreach (var p in polys.Values) {
                var polyVerts = p.VertexIds.Select(v => vertices[(ushort)v].Origin).ToArray();

                for (int i = 2; i < polyVerts.Length; i++) {
                    var a = polyVerts[0];
                    var b = polyVerts[i - 1];
                    var c = polyVerts[i];
                    var norm = CalculateTriSurfaceNormal(a, b, c);

                    if (norm.Z < walkableThr)
                        continue;

                    tris.Add(new List<Vector3>() { a, b, c }.ToList());
                }
            }

            var subject = new Clipper2Lib.PathsD(tris.Select(t => {
                return new Clipper2Lib.PathD(t.Select(v => new Clipper2Lib.PointD(v.X, v.Y)));
            }).ToList());

            var paths = Clipper2Lib.Clipper.Union(subject, new Clipper2Lib.PathsD(), Clipper2Lib.FillRule.Positive, 2);

            if (paths.Count > 0) {
                foreach (var path in paths) {
                    if (path.Count > 0) {
                        var pb = new SvgPathBuilder();

                        pb.AddMoveTo(false, (float)path.First().x, (float)-path.First().y);
                        foreach (var point in path.Skip(1)) {
                            pb.AddLineTo(false, (float)point.x, (float)-point.y);
                        }
                        pb.AddLineTo(false, (float)path.First().x, (float)-path.First().y);

                        floorsGroup.Children.Add(new SvgPathElement() {
                            PathData = pb.ToPathData(),
                            Class = cell.CellGeometry.IsCatwalk ? "catwalk" : "floor"
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Calculate the surface normal of a triangle
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public static Vector3 CalculateTriSurfaceNormal(Vector3 a, Vector3 b, Vector3 c) {
            return Vector3.Normalize(Vector3.Cross(b - a, c - a));
        }

        private void BuildStaticObjects(SvgGroupElement stabsGroup, MapCellGeometry cell) {
            foreach (var stab in cell.CellGeometry.EnvCell.StaticObjects) {
                var pos = new Vector2(stab.Frame.Origin.X, -stab.Frame.Origin.Y);
                var rot = (float)(QuaternionToHeading(stab.Frame.Orientation));
                var world = Matrix3x2.CreateRotation(rot) * Matrix3x2.CreateTranslation(pos);
                var s = BuildStab(stabsGroup, cell, stab.Id);
                if (s != null) {
                    stabsGroup.Tag = cell.Position.Z;
                    s.Transform = new List<SvgTransform>() {
                        new SvgMatrixTransform() {
                            Matrix = new GrapeCity.Documents.Common.Matrix(world)
                        }
                    };
                    stabsGroup.Children.Add(s);
                }
            }
        }

        private SvgElement BuildStab(SvgGroupElement stabsGroup, MapCellGeometry cell, uint stabId) {
            if ((stabId & 0x01000000) != 0) {
                return BuildGfxObj(cell, stabId, Matrix4x4.Identity);
            }
            else if ((stabId & 0x02000000) != 0) {
                return BuildStaticObj(cell, stabId);
            }

            return null;
        }

        private SvgElement BuildStaticObj(MapCellGeometry cell, uint id) {
            if (!_staticCache.TryGetValue(id, out SvgGroupElement stabGroup)) {
                stabGroup = new SvgGroupElement() {
                    ID = $"0x{id:X8}"
                };
                var staticObj = DatManager.PortalDat.ReadFromDat<SetupModel>(id);

                var partIndexes = Enumerable.Range(0, staticObj.Parts.Count).ToList();

                partIndexes.Sort((a, b) => {
                    var az = staticObj.PlacementFrames.Last().Value.AnimFrame.Frames[a].Origin.Z;
                    var bz = staticObj.PlacementFrames.Last().Value.AnimFrame.Frames[b].Origin.Z;

                    return az.CompareTo(bz);
                });

                for (var i = 0; i < staticObj.Parts.Count; i++) {
                    var frame = staticObj.PlacementFrames.Last().Value.AnimFrame.Frames[partIndexes[i]];
                    var pos = Matrix4x4.CreateTranslation(frame.Origin);
                    var rot = Matrix4x4.CreateFromQuaternion(frame.Orientation);

                    var s = BuildGfxObj(cell, staticObj.Parts[partIndexes[i]], rot * pos);
                    if (s != null)
                        stabGroup.Children.Add(s);
                }

                if (!stabGroup.HasChildren)
                    return null;

                _staticCache.Add(id, stabGroup);
            }

            return new SvgUseElement() {
                Href = new SvgReference($"0x{id:X8}")
            };
        }

        private SvgElement BuildGfxObj(MapCellGeometry cell, uint id, Matrix4x4 world) {
            var gfxObjGroup = new SvgGroupElement() {
                    CustomAttributes = new List<SvgCustomAttribute>() {
                        new SvgCustomAttribute("data-gfxobj-id", $"0x{id:X8}")
                    }
                };

            var gfxObj = DatManager.PortalDat.ReadFromDat<GfxObj>(id);

            if (gfxObj.Polygons.Count == 0) {
                return null;
            }

            var polys = gfxObj.Polygons.Values.ToList();
            polys.Sort((a, b) => {
                return gfxObj.VertexArray.Vertices[(ushort)a.VertexIds.First()].Origin.Z.CompareTo(gfxObj.VertexArray.Vertices[(ushort)b.VertexIds.First()].Origin.Z);
            });

            foreach (var p in polys) {
                var polyVerts = p.VertexIds.Select(v => gfxObj.VertexArray.Vertices[(ushort)v].Origin).ToArray();

                var pb = new SvgPathBuilder();


                var v1 = Vector3.Transform(polyVerts.First(), world);
                pb.AddMoveTo(false, v1.X, -v1.Y);
                foreach (var point in polyVerts.Skip(1)) {
                    var v = Vector3.Transform(point, world);
                    pb.AddLineTo(false, v.X, -v.Y);
                }
                pb.AddLineTo(false, v1.X, -v1.Y);

                gfxObjGroup.Children.Add(new SvgPathElement() {
                    PathData = pb.ToPathData(),
                    Class = "static"
                });
            }

            if (!gfxObjGroup.HasChildren)
                return null;
            return gfxObjGroup;
        }

        private void BuildWalls(SvgGroupElement wallsGroup, MapCellGeometry cell) {
            var cellStructKey = cell.CellGeometry.EnvCell.CellStructure;
            if (!cell.CellGeometry.Environment.Cells.ContainsKey(cellStructKey))
                return;

            var cellStruct = cell.CellGeometry.Environment.Cells[cellStructKey];
            var polys = cellStruct.PhysicsPolygons;
            var vertices = cellStruct.VertexArray.Vertices;

            if (polys.Count == 0) {
                return;
            }

            var pb = new SvgPathBuilder();

            foreach (var kv in polys) {
                if (cell.CellGeometry.WalkablePolyIds[cellStructKey].Contains(kv.Key))
                    continue;

                var p = kv.Value;

                var verts = new List<Vector3>();
                for (int i = 0; i < p.VertexIds.Count; i++) {
                    var i1 = p.VertexIds[i];
                    var i2 = p.VertexIds[i == 0 ? p.VertexIds.Count - 1 : i - 1];

                    var v1 = vertices[(ushort)i1].Origin;
                    var v2 = vertices[(ushort)i2].Origin;

                    var d = 0.1f;

                    foreach (var plane in cell.CellGeometry.WalkablePlanes) {
                        if (DistanceToPlane(v1, plane) < d && DistanceToPlane(v2, plane) < d && !(verts.Contains(v1) && verts.Contains(v2))) {
                            verts.Add(v1);
                            verts.Add(v2);
                            break;
                        }
                    }
                }

                if (verts.Count > 0) {
                    pb.AddMoveTo(false, verts.First().X, -verts.First().Y);
                    foreach (var vert in verts) {
                        pb.AddLineTo(false, vert.X, -vert.Y);
                    }
                }
            }

            var pathData = pb.ToPathData();

            if (pathData.FigureCount == 0)
                return;

            var handles = new SvgPathElement() {
                Class = "wall",
                PathData = pathData
            };
            wallsGroup.Children.Add(handles);
        }

        Vector3 ClosestPointOnPlane(Vector3 point, Plane plane) {
            var pointToPlaneDistance = Vector3.Dot(plane.Normal, point) + plane.D;
            return point - (plane.Normal * pointToPlaneDistance);
        }

        float DistanceToPlane(Vector3 point, Plane plane) {
            var closest = ClosestPointOnPlane(point, plane);

            return Math.Abs(Vector3.Distance(point, closest));
        }

        double QuaternionToHeading(Quaternion q) {
            double sinr = 2 * (q.W * -q.Z + q.X * q.Y);
            double cosr = 1 - 2 * (q.Y * q.Y + -q.Z * -q.Z);
            return Math.Atan2(sinr, cosr);
        }

        public string ToSVGString() {
            var sb = new StringBuilder();
            svg.Save(sb, new System.Xml.XmlWriterSettings() {
                Indent = true,
            });
            return sb.ToString()
                .Replace(" href=", " xlink:href=")
                .Replace(" xmlns=\"http://www.w3.org/2000/svg\"", " xmlns=\"http://www.w3.org/2000/svg\"  xmlns:xlink=\"http://www.w3.org/1999/xlink\"")
                .Replace("</defs>", $"{BuildFilter()}</defs>");
        }

        private object BuildFilter() {
            return @"
  <filter id=""ramp-shadow"">
    <feDropShadow dx=""0"" dy=""0"" stdDeviation=""1"" flood-color=""rgb(20,20,20)"" />
  </filter>
  ";
        }

        public void Dispose() {
            svg?.Dispose();
        }
    }
}
