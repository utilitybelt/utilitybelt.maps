﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UtilityBelt.Navigation.Geometry
{
    public enum CellType
    {
        Terrain,
        Indoors,
        Dungeon
    }
}
