﻿using System.Collections.Generic;

namespace UtilityBelt.Navigation.Data {
    public class DungeonInfo {
        public string Name { get; set; } = "Unknown";

        public uint LandblockId { get; set; }

        public List<uint> CellList { get; set; } = new List<uint>();
    }
}
