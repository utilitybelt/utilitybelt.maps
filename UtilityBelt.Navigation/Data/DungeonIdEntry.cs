﻿namespace UtilityBelt.Navigation.Data {
    public class DungeonIdEntry {
        public string? Id { get; set; }

        public string Name { get; set; } = "Unknown";

        public int Priority { get; set; }
    }
}
