﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityBelt.Navigation.Data {
    public static class Dungeons {
        private static Task<DungeonIdEntry[]?>? _dungeonIdEntries;
        private static Task<Dictionary<string, Dictionary<string, List<string>>>?>? _dungeonCells;
        private static DungeonInfo[]? _dungeonInfoCache;
        private static Dictionary<uint, string> dungeonNames;

        private static Dictionary<uint, string> DungeonNames {
            get {
                if (dungeonNames == null) {
                    using (var stream = typeof(Dungeons).Assembly.GetManifestResourceStream($"UtilityBelt.Navigation.Resources.dungeons.csv")) {
                        dungeonNames = new Dictionary<uint, string>();

                        using (var reader = new StreamReader(stream, true)) {
                            string line;
                            while ((line = reader.ReadLine()) != null) {
                                var parts = line.Split(',');
                                if (parts.Length != 2) continue;

                                if (uint.TryParse(parts[0], System.Globalization.NumberStyles.HexNumber, null, out uint parsed)) {
                                    if (!dungeonNames.ContainsKey(parsed)) {
                                        dungeonNames[parsed << 16] = parts[1];
                                    }
                                }
                            }
                        }
                    }
                }

                return dungeonNames;
            }
        }

        private static Task<DungeonIdEntry[]?> GetDungeonIds() {
            if (_dungeonIdEntries != null) {
                return _dungeonIdEntries;
            }
            var contractResolver = new DefaultContractResolver {
                NamingStrategy = new CamelCaseNamingStrategy()
            };

            using (var stream = typeof(Dungeons).Assembly.GetManifestResourceStream($"UtilityBelt.Navigation.Resources.dungeonIds.json")) {
                using (var reader = new StreamReader(stream, true)) {
                    var res = JsonConvert.DeserializeObject<DungeonIdEntry[]>(reader.ReadToEnd(), new JsonSerializerSettings() {
                        ContractResolver = contractResolver
                    });

                    _dungeonIdEntries = Task.FromResult(res);
                }
            }

            return _dungeonIdEntries;
        }

        private static Task<Dictionary<string, Dictionary<string, List<string>>>> GetDungeonCells() {
            if (_dungeonCells != null) {
                return _dungeonCells;
            }

            var contractResolver = new DefaultContractResolver {
                NamingStrategy = new CamelCaseNamingStrategy()
            };

            using (var stream = typeof(Dungeons).Assembly.GetManifestResourceStream($"UtilityBelt.Navigation.Resources.dungeons.json")) {
                using (var reader = new StreamReader(stream, true)) {
                    var res = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, List<string>>>>(reader.ReadToEnd(), new JsonSerializerSettings() {
                        ContractResolver = contractResolver
                    });

                    _dungeonCells = Task.FromResult(res);
                }
            }


            return _dungeonCells;
        }

        public static DungeonInfo[] GetDungeonInfos() {
            if (_dungeonInfoCache is not null) {
                return _dungeonInfoCache;
            }

            var dungeonIds = GetDungeonIds();
            var dungeonCells = GetDungeonCells();

            Task.WaitAll(dungeonIds, dungeonCells);

            if (dungeonIds.Result == null || dungeonCells.Result == null) {
                return new DungeonInfo[] { };
            }

            var idLookup = new Dictionary<string, DungeonIdEntry>();
            foreach (var dungeonEntry in dungeonIds.Result) {
                if (dungeonEntry.Id is not null) {
                    idLookup.Add(dungeonEntry.Id, dungeonEntry);
                }
            }

            var infos = new Dictionary<string, DungeonInfo>();
            foreach (var dungeonKV in dungeonCells.Result) {
                var lbId = uint.Parse(dungeonKV.Key, System.Globalization.NumberStyles.HexNumber) << 16;
                foreach (var dungeonCellKV in dungeonKV.Value) {
                    var cellId = uint.Parse(dungeonCellKV.Key, System.Globalization.NumberStyles.HexNumber);
                    var idEntries = dungeonCellKV.Value.Select(v => idLookup[v]).ToList();

                    idEntries.Sort((a, b) => a.Priority.CompareTo(b.Priority));

                    var infoKey = string.Join(".", idEntries.Select(e => e.Id));

                    if (!infos.TryGetValue(infoKey, out DungeonInfo dungeonInfo)) {
                        dungeonInfo = new DungeonInfo() {
                            LandblockId = lbId,
                            Name = string.Join(" - ", idEntries.Select(e => e.Name)),
                            CellList = new List<uint>()
                        };
                        infos.Add(infoKey, dungeonInfo);
                    }
                    dungeonInfo.CellList.Add(cellId);
                }
            }

            var cache = infos.Values.ToList();

            foreach (var kv in DungeonNames) {
                if (!cache.Any(d => (d.LandblockId & 0xFFFF0000) == kv.Key)) {
                    cache.Add(new DungeonInfo() {
                        LandblockId = kv.Key,
                        CellList = new List<uint>() { 0 },
                        Name = kv.Value
                    });
                }
            }

            _dungeonInfoCache = cache.ToArray();

            return _dungeonInfoCache;
        }

        public static bool TryParseLandblockId(string landblockId, out uint landcellId) {
            if (landblockId.StartsWith("0x")) {
                if (!uint.TryParse(landblockId.Replace("0x", ""), System.Globalization.NumberStyles.HexNumber, null, out uint parsedLandblockId)) {
                    landcellId = 0;
                    return false;
                }
                landcellId = parsedLandblockId;
                return true;
            }
            else {
                if (!uint.TryParse(landblockId, out uint parsedLandblockId)) {
                    landcellId = 0;
                    return false;
                }
                landcellId = parsedLandblockId;
                return true;
            }
        }
    }
}
